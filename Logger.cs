﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AllegroWebApiBenchmark
{
    class Logger : ILogger
    {
        private readonly string _fileName;
        private readonly BlockingCollection<string> _messages;
        private readonly Task _messagesSaver;

        public Logger(string fileName)
        {
            _fileName = fileName;
            _messages = new BlockingCollection<string>();
            _messagesSaver = SaveMessagesToFilePeriodicallyAsync(TimeSpan.FromSeconds(10), new CancellationToken());
        }

        public void LogMessage(string message, bool saveToFile = true)
        {
            var currentTime = DateTime.Now;
            var messageToWrite = $"{currentTime} {message}";

            if (saveToFile) _messages.Add(messageToWrite);
            Console.WriteLine(messageToWrite);
        }

        public async Task SavePendingMessagesToFileAsync()
        {
            await SaveMessagesToFileAsync();
        }

        private async Task SaveMessagesToFilePeriodicallyAsync(TimeSpan interval, CancellationToken cancellationToken)
        {
            while (true)
            {
                await SaveMessagesToFileAsync();
                await Task.Delay(interval, cancellationToken);
                if (cancellationToken.IsCancellationRequested)
                    break;
            }
        }

        private async Task SaveMessagesToFileAsync()
        {
            if (_messages.Count == 0) return;

            var sb = new StringBuilder();
            while (_messages.Any())
            {
                var message = _messages.Take();
                sb.AppendLine(message);
            }

            await File.AppendAllTextAsync(_fileName, sb.ToString());
        }
    }
}
