﻿using System.Threading.Tasks;
using AllegroWebApiBenchmark.AllegroWebApiServiceGenerated;

namespace AllegroWebApiBenchmark
{
    public interface IAllegroWebApiService
    {
        Task<doGetBidItem2Response> doGetBidItem2Async(doGetBidItem2Request request);
        Task<doGetItemsInfoResponse> doGetItemsInfoAsync(doGetItemsInfoRequest request);
        Task<doGetSiteJournalResponse> doGetSiteJournalAsync(doGetSiteJournalRequest request);
        Task<doLoginEncResponse> doLoginEncAsync(doLoginEncRequest request);
        Task<doQueryAllSysStatusResponse> doQueryAllSysStatusAsync(doQueryAllSysStatusRequest request);
    }
}