﻿using System;

namespace AllegroWebApiBenchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            var credentials = new Credentials(@"C:\Users\dawid.rychlowski\Desktop\allegroAccount.txt");
            var logger = new Logger("logs.txt");

            //var service = new AllegroWebApiServiceGenerated.AllegroWebApiServiceGenerated();
            //var client = new AllegroWebApiClient(service, credentials, logger);
            //client.TestGetItemsWithGetBidItemForErrorsAsync(TimeSpan.FromMinutes(60), 15).Wait();

            var customService = new AllegroWebApiService();
            var client = new AllegroWebApiClient(customService, credentials, logger);
            client.TestGetItemsWithGetBidItemForErrorsAsync(TimeSpan.FromMinutes(10), 15).Wait();

            Console.ReadKey();
        }
    }
}