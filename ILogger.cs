﻿using System.Threading.Tasks;

namespace AllegroWebApiBenchmark
{
    public interface ILogger
    {
        void LogMessage(string message, bool saveToFile = true);
        Task SavePendingMessagesToFileAsync();
    }
}
