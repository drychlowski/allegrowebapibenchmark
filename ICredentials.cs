﻿namespace AllegroWebApiBenchmark
{
    public interface ICredentials
    {
        string Username { get; }
        string Password { get; }
        string WebApiKey { get; }
    }
}
