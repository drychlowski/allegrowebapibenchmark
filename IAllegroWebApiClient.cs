﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AllegroWebApiBenchmark.AllegroWebApiServiceGenerated;

namespace AllegroWebApiBenchmark
{
    public interface IAllegroWebApiClient
    {
        Task UpdateVersionKeyAsync();
        Task UpdateSessionAsync();
        Task<SiteJournal[]> GetJournalAsync(long startingPoint);
        Task<ItemInfoStruct[]> GetItemsInfoAsync(long[] itemsIdArray);
        Task<BidListStruct2[]> GetBidItemAsync(long itemId);
        Task TestGetItemsWithGetBidItemForErrorsAsync(TimeSpan testTimeSpan, int parallelTaskCount);
    }
}