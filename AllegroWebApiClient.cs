﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AllegroWebApiBenchmark.AllegroWebApiServiceGenerated;

namespace AllegroWebApiBenchmark
{
    public class AllegroWebApiClient : IAllegroWebApiClient
    {
        private readonly IAllegroWebApiService _service;
        private readonly ICredentials _credentials;
        private readonly ILogger _logger;
        private const int CountryId = 1;
        private long _versionKey;
        private string _session;

        public AllegroWebApiClient(IAllegroWebApiService service, ICredentials credentials, ILogger logger)
        {
            _service = service;
            _credentials = credentials;
            _logger = logger;
            _session = string.Empty;
        }

        public async Task UpdateVersionKeyAsync()
        {
            var request = new doQueryAllSysStatusRequest(CountryId, _credentials.WebApiKey);

            var response = await _service.doQueryAllSysStatusAsync(request);
            if (response != null) _versionKey = response.sysCountryStatus[0].verKey;
        }

        public async Task UpdateSessionAsync()
        {
            string encodedPassword;
            using (SHA256 sha256 = new SHA256Managed())
            {
                var passwordHash = sha256.ComputeHash(Encoding.ASCII.GetBytes(_credentials.Password));
                encodedPassword = Convert.ToBase64String(passwordHash);
            }

            var request = new doLoginEncRequest(_credentials.Username, encodedPassword, CountryId, _credentials.WebApiKey, _versionKey);
            doLoginEncResponse response;
            try
            {
                response = await _service.doLoginEncAsync(request);
                _session = response?.sessionHandlePart;
            }
            catch (FaultException e)
            {
                if (e.Code.Name.Equals("ERR_INVALID_VERSION_CAT_SELL_FIELDS"))
                {
                    await Retry.DoAsync(UpdateVersionKeyAsync, TimeSpan.FromSeconds(1));
                    request.localVersion = _versionKey;
                    response = await _service.doLoginEncAsync(request);
                    _session = response?.sessionHandlePart;
                }
                else throw;
            }
        }

        public async Task<SiteJournal[]> GetJournalAsync(long startingPoint)
        {
            var request = new doGetSiteJournalRequest(_session, startingPoint, 1);
            doGetSiteJournalResponse response;
            try
            {
                response = await _service.doGetSiteJournalAsync(request);
                return response?.siteJournalArray;
            }
            catch (FaultException e)
            {
                if (e.Code.Name.Equals("ERR_NO_SESSION") || e.Code.Name.Equals("ERR_SESSION_EXPIRED"))
                {
                    await Retry.DoAsync(UpdateSessionAsync, TimeSpan.FromSeconds(1));
                    request.sessionHandle = _session;
                    response = await _service.doGetSiteJournalAsync(request);
                    return response?.siteJournalArray;
                }
                else throw;
            }
        }

        public async Task<ItemInfoStruct[]> GetItemsInfoAsync(long[] itemsIdArray)
        {
            var request = new doGetItemsInfoRequest(_session, itemsIdArray, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            doGetItemsInfoResponse response;
            try
            {
                response = await _service.doGetItemsInfoAsync(request);
                return response?.arrayItemListInfo;
            }
            catch (FaultException e)
            {
                if (e.Code.Name.Equals("ERR_NO_SESSION") || e.Code.Name.Equals("ERR_SESSION_EXPIRED"))
                {
                    await Retry.DoAsync(UpdateSessionAsync, TimeSpan.FromSeconds(1));
                    response = await _service.doGetItemsInfoAsync(request);
                    return response?.arrayItemListInfo;
                }
                else throw;
            }
        }

        public async Task<BidListStruct2[]> GetBidItemAsync(long itemId)
        {
            var request = new doGetBidItem2Request(_session, itemId);
            doGetBidItem2Response response;
            try
            {
                response = await _service.doGetBidItem2Async(request);
                return response?.biditemList;
            }
            catch (FaultException e)
            {
                if (e.Code.Name.Equals("ERR_NO_SESSION") || e.Code.Name.Equals("ERR_SESSION_EXPIRED"))
                {
                    await Retry.DoAsync(UpdateSessionAsync, TimeSpan.FromSeconds(1));
                    response = await _service.doGetBidItem2Async(request);
                    return response?.biditemList;
                }
                if (e.Code.Name.Equals("ERR_AUCTION_KILLED"))
                {
                    return null;
                }
                else throw;
            }
        }

        public async Task<long[]> CountGetItemsInfoWithGetBidItemErrorAsync(CancellationToken token, int taskId)
        {
            long errorCount = 0;
            long requestCount = 0;
            long startingPoint = 0;

            while (!token.IsCancellationRequested)
            {
                SiteJournal[] journal = null;
                try
                {
                    _logger.LogMessage($"Task {taskId} is processing {requestCount} request.", false);
                    journal = await GetJournalAsync(startingPoint);
                    requestCount++;

                    var itemsWithStartChangeStatusId = journal
                        .Where(j => new[] {"start", "change"}.Contains(j.changeType))
                        .Select(j => j.itemId)
                        .ToArray();

                    var itemsWithEndBidNowCancelStatusId = journal
                        .Where(j => new[] {"end", "bid", "now", "cancel_bid"}.Contains(j.changeType))
                        .Select(j => j.itemId)
                        .ToArray();

                    while (itemsWithStartChangeStatusId.Any())
                    {
                        var ids = itemsWithStartChangeStatusId.Take(25).ToArray();

                        _logger.LogMessage($"Task {taskId} is processing {requestCount} request.", false);
                        var itemsInfo = await GetItemsInfoAsync(ids);
                        requestCount++;

                        itemsWithStartChangeStatusId = itemsWithStartChangeStatusId.Skip(25).ToArray();
                    }

                    while (itemsWithEndBidNowCancelStatusId.Any())
                    {
                        var ids = itemsWithEndBidNowCancelStatusId.Take(25).ToArray();

                        _logger.LogMessage($"Task {taskId} is processing {requestCount} request.", false);
                        var itemsInfo = await GetItemsInfoAsync(ids);
                        requestCount++;

                        foreach (var id in ids)
                        {
                            _logger.LogMessage($"Task {taskId} is processing {requestCount} request.", false);
                            var usersInfo = await GetBidItemAsync(id);
                            requestCount++;
                        }

                        itemsWithEndBidNowCancelStatusId = itemsWithEndBidNowCancelStatusId.Skip(25).ToArray();
                    }
                }
                catch (FaultException e)
                {
                    errorCount++;
                    _logger.LogMessage($"Task {taskId} throws ERROR during processing {requestCount} request. {e.Code.Name}");
                    requestCount++;
                }
                catch (Exception e)
                {
                    errorCount++;
                    while (e.InnerException != null) e = e.InnerException;
                    _logger.LogMessage($"Task {taskId} throws ERROR during processing {requestCount} request. {e.Message}");
                    requestCount++;
                }

                if (journal != null) startingPoint = journal.Last().rowId;
            }

            return new []{ requestCount, errorCount};
        }

        public async Task TestGetItemsWithGetBidItemForErrorsAsync(TimeSpan testTimeSpan, int parallelTaskCount)
        {
            var tasks = new List<Task<long[]>>(parallelTaskCount);
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            _logger.LogMessage($"Start processing requests for {testTimeSpan.TotalMinutes} minutes.");

            for (var i = 0; i < parallelTaskCount; i++)
            {
                tasks.Add(CountGetItemsInfoWithGetBidItemErrorAsync(token, i));
            }

            tokenSource.CancelAfter(testTimeSpan);

            var result = await Task.WhenAll(tasks);

            var requestCount = result.Select(o => o[0]).Sum();
            var errorCount = result.Select(o => o[1]).Sum();
            var errorRate = errorCount * 100.0 / requestCount;

            _logger.LogMessage($"Request count: {requestCount}. Error count: {errorCount}. Error rate: {Math.Round(errorRate, 2)}%.");
            await _logger.SavePendingMessagesToFileAsync();
        }
    }
}