﻿using System.Linq;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using AllegroWebApiBenchmark.AllegroWebApiServiceGenerated;

namespace AllegroWebApiBenchmark
{
    public class AllegroWebApiService : IAllegroWebApiService
    {
        private readonly HttpClient _client;

        public AllegroWebApiService()
        {
            _client = new HttpClient();
        }

        public async Task<doQueryAllSysStatusResponse> doQueryAllSysStatusAsync(doQueryAllSysStatusRequest request)
        {
            var soapString = $@"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <DoQueryAllSysStatusRequest xmlns=""https://webapi.allegro.pl/service.php"">
                                            <countryId>{request.countryId}</countryId>
                                            <webapiKey>{request.webapiKey}</webapiKey>
                                        </DoQueryAllSysStatusRequest>
                                    </s:Body>
                                </s:Envelope>";

            var response = await HandleResponse(soapString);
            return Deserialize<doQueryAllSysStatusResponse>(response);
        }

        public async Task<doGetBidItem2Response> doGetBidItem2Async(doGetBidItem2Request request)
        {
            var soapString = $@"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <DoGetBidItem2Request xmlns=""https://webapi.allegro.pl/service.php"">
                                            <sessionHandle>{request.sessionHandle}</sessionHandle>
                                            <itemId>{request.itemId}</itemId>
                                        </DoGetBidItem2Request>
                                    </s:Body>
                                </s:Envelope>";

            var response = await HandleResponse(soapString);
            return Deserialize<doGetBidItem2Response>(response);
        }

        public async Task<doGetItemsInfoResponse> doGetItemsInfoAsync(doGetItemsInfoRequest request)
        {
            var items = request.itemsIdArray;
            var sb = new StringBuilder();

            foreach (var item in items)
            {
                sb.Append($"<item>{item}</item>");
            }
            var soapItems = sb.ToString();

            var soapString = $@"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <DoGetItemsInfoRequest xmlns=""https://webapi.allegro.pl/service.php"">
                                            <sessionHandle>{request.sessionHandle}</sessionHandle>
                                            <itemsIdArray>{soapItems}</itemsIdArray>
                                            <getDesc>{request.getDesc}</getDesc>
                                            <getImageUrl>{request.getImageUrl}</getImageUrl>
                                            <getAttribs>{request.getAttribs}</getAttribs>
                                            <getPostageOptions>{request.getPostageOptions}</getPostageOptions>
                                            <getCompanyInfo>{request.getCompanyInfo}</getCompanyInfo>
                                            <getProductInfo>{request.getProductInfo}</getProductInfo>
                                            <getAfterSalesServiceConditions>{request.getAfterSalesServiceConditions}</getAfterSalesServiceConditions>
                                            <getEan>{request.getEan}</getEan>
                                            <getAdditionalServicesGroup>{request.getAdditionalServicesGroup}</getAdditionalServicesGroup>
                                        </DoGetItemsInfoRequest>
                                    </s:Body>
                                </s:Envelope>";

            var response = await HandleResponse(soapString);
            return Deserialize<doGetItemsInfoResponse>(response);
        }

        public async Task<doGetSiteJournalResponse> doGetSiteJournalAsync(doGetSiteJournalRequest request)
        {
            var soapString = $@"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <DoGetSiteJournalRequest xmlns=""https://webapi.allegro.pl/service.php"">
                                            <sessionHandle>{request.sessionHandle}</sessionHandle>
                                            <startingPoint>{request.startingPoint}</startingPoint>
                                            <infoType>{request.infoType}</infoType>
                                        </DoGetSiteJournalRequest>
                                    </s:Body>
                                </s:Envelope>";

            var response = await HandleResponse(soapString);
            return Deserialize<doGetSiteJournalResponse>(response);
        }

        public async Task<doLoginEncResponse> doLoginEncAsync(doLoginEncRequest request)
        {
            var soapString = $@"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                                        <DoLoginEncRequest xmlns=""https://webapi.allegro.pl/service.php"">
                                            <userLogin>{request.userLogin}</userLogin>
                                            <userHashPassword>{request.userHashPassword}=</userHashPassword>
                                            <countryCode>{request.countryCode}</countryCode>
                                            <webapiKey>{request.webapiKey}</webapiKey>
                                            <localVersion>{request.localVersion}</localVersion>
                                        </DoLoginEncRequest>
                                    </s:Body>
                                </s:Envelope>";

            var response = await HandleResponse(soapString);
            return Deserialize<doLoginEncResponse>(response);
        }

        private T Deserialize<T>(XDocument response)
        {
            XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
            var responseToDeserialize = response.Descendants(ns + "Body").First().FirstNode;

            var deserializer = new XmlSerializer(typeof(T));
            var responseObj = (T)deserializer.Deserialize(responseToDeserialize.CreateReader());

            return responseObj;
        }

        private async Task<XDocument> HandleResponse(string soapString)
        {
            var content = new StringContent(soapString, Encoding.UTF8, "text/xml");

            using (var httpResponse = await _client.PostAsync("https://webapi.allegro.pl/service.php", content))
            {
                var soapResponse = await httpResponse.Content.ReadAsStringAsync();
                var response = XDocument.Parse(soapResponse);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    var faultCode = response.Descendants("faultcode").First().Value;
                    var faulsMessage = response.Descendants("faultstring").First().Value;
                    var exception = new FaultException(new FaultReason(faulsMessage), new FaultCode(faultCode), null);
                    throw exception;
                }

                return response;
            }
        }
    }
}