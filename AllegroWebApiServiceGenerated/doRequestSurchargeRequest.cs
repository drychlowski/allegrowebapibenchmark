﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoRequestSurchargeRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doRequestSurchargeRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public string surchargeMessageToBuyer;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public float surchargeValue;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long transactionId;

        public doRequestSurchargeRequest()
        {
        }

        public doRequestSurchargeRequest(string sessionHandle, long transactionId, float surchargeValue,
            string surchargeMessageToBuyer)
        {
            this.sessionHandle = sessionHandle;
            this.transactionId = transactionId;
            this.surchargeValue = surchargeValue;
            this.surchargeMessageToBuyer = surchargeMessageToBuyer;
        }
    }
}