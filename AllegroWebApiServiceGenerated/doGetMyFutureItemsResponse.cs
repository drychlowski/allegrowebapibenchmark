﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyFutureItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyFutureItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int futureItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public FutureItemStruct[] futureItemsList;

        public doGetMyFutureItemsResponse()
        {
        }

        public doGetMyFutureItemsResponse(int futureItemsCounter, FutureItemStruct[] futureItemsList)
        {
            this.futureItemsCounter = futureItemsCounter;
            this.futureItemsList = futureItemsList;
        }
    }
}