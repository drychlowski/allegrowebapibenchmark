﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyPayoutsDetailsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyPayoutsDetailsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public PayoutPaymentsStruct[] payments;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int paymentsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        [XmlArrayItem("item", IsNullable = false)]
        public PayoutRefundFromStruct[] refundFrom;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int refundsFromCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int refundsToCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        [XmlArrayItem("item", IsNullable = false)]
        public PayoutRefundToStruct[] refundTo;

        public doGetMyPayoutsDetailsResponse()
        {
        }

        public doGetMyPayoutsDetailsResponse(int paymentsCount, PayoutPaymentsStruct[] payments, int refundsFromCount,
            PayoutRefundFromStruct[] refundFrom, int refundsToCount, PayoutRefundToStruct[] refundTo)
        {
            this.paymentsCount = paymentsCount;
            this.payments = payments;
            this.refundsFromCount = refundsFromCount;
            this.refundFrom = refundFrom;
            this.refundsToCount = refundsToCount;
            this.refundTo = refundTo;
        }
    }
}