﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoCancelBidItemRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doCancelBidItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long cancelAddToBlackList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] cancelBidsArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public string cancelBidsReason;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long cancelItemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doCancelBidItemRequest()
        {
        }

        public doCancelBidItemRequest(string sessionHandle, long cancelItemId, int[] cancelBidsArray,
            string cancelBidsReason, long cancelAddToBlackList)
        {
            this.sessionHandle = sessionHandle;
            this.cancelItemId = cancelItemId;
            this.cancelBidsArray = cancelBidsArray;
            this.cancelBidsReason = cancelBidsReason;
            this.cancelAddToBlackList = cancelAddToBlackList;
        }
    }
}