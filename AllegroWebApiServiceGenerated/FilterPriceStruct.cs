﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FilterPriceStruct
    {
        /// <remarks />
        public float filterPriceFrom { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterPriceFromSpecified { get; set; }

        /// <remarks />
        public float filterPriceTo { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterPriceToSpecified { get; set; }
    }
}