﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PayoutAutoSettingsStruct
    {
        /// <remarks />
        public float payoutAutoAmount { get; set; }

        /// <remarks />
        public PayoutAutoFrequencyStruct payoutAutoFrequency { get; set; }
    }
}