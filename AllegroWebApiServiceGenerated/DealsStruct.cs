﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class DealsStruct
    {
        /// <remarks />
        public long dealId { get; set; }

        /// <remarks />
        public long dealDate { get; set; }

        /// <remarks />
        public int dealQuantity { get; set; }

        /// <remarks />
        public float dealAmountOriginal { get; set; }

        /// <remarks />
        public float dealAmountDiscounted { get; set; }
    }
}