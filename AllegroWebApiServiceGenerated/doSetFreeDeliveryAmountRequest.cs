﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoSetFreeDeliveryAmountRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSetFreeDeliveryAmountRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int activeFlag;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public float freeDeliveryAmount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        public doSetFreeDeliveryAmountRequest()
        {
        }

        public doSetFreeDeliveryAmountRequest(string sessionId, int activeFlag, float freeDeliveryAmount)
        {
            this.sessionId = sessionId;
            this.activeFlag = activeFlag;
            this.freeDeliveryAmount = freeDeliveryAmount;
        }
    }
}