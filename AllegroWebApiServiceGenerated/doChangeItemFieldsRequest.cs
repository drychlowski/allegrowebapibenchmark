﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoChangeItemFieldsRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doChangeItemFieldsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 8)]
        public string additionalServicesGroup;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public AfterSalesServiceConditionsStruct afterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public FieldsValue[] fieldsToModify;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] fieldsToRemove;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int previewOnly;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        [XmlArrayItem("item", IsNullable = false)]
        public TagNameStruct[] tags;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        [XmlArrayItem("item", IsNullable = false)]
        public VariantStruct[] variants;

        public doChangeItemFieldsRequest()
        {
        }

        public doChangeItemFieldsRequest(string sessionId, long itemId, FieldsValue[] fieldsToModify,
            int[] fieldsToRemove, int previewOnly, VariantStruct[] variants, TagNameStruct[] tags,
            AfterSalesServiceConditionsStruct afterSalesServiceConditions, string additionalServicesGroup)
        {
            this.sessionId = sessionId;
            this.itemId = itemId;
            this.fieldsToModify = fieldsToModify;
            this.fieldsToRemove = fieldsToRemove;
            this.previewOnly = previewOnly;
            this.variants = variants;
            this.tags = tags;
            this.afterSalesServiceConditions = afterSalesServiceConditions;
            this.additionalServicesGroup = additionalServicesGroup;
        }
    }
}