﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMySellItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMySellItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int sellItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public SellItemStruct[] sellItemsList;

        public doGetMySellItemsResponse()
        {
        }

        public doGetMySellItemsResponse(int sellItemsCounter, SellItemStruct[] sellItemsList)
        {
            this.sellItemsCounter = sellItemsCounter;
            this.sellItemsList = sellItemsList;
        }
    }
}