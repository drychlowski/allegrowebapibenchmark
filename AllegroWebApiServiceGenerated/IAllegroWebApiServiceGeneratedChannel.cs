﻿using System.CodeDom.Compiler;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    public interface IAllegroWebApiServiceGeneratedChannel : IAllegroWebApiServiceGenerated, IClientChannel
    {
    }
}