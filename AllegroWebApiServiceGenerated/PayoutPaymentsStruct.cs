﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PayoutPaymentsStruct
    {
        /// <remarks />
        public long tranasctionId { get; set; }

        /// <remarks />
        public string userName { get; set; }

        /// <remarks />
        public long userId { get; set; }

        /// <remarks />
        public float amount { get; set; }

        /// <remarks />
        public float transportAmount { get; set; }

        /// <remarks />
        public float totalAmount { get; set; }

        /// <remarks />
        public string paidDate { get; set; }
    }
}