﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetPostBuyFormsIdsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetPostBuyFormsIdsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public FiltersListType[] filtersList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int formsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] formsIds;

        public doGetPostBuyFormsIdsResponse()
        {
        }

        public doGetPostBuyFormsIdsResponse(int formsCount, long[] formsIds, FiltersListType[] filtersList)
        {
            this.formsCount = formsCount;
            this.formsIds = formsIds;
            this.filtersList = filtersList;
        }
    }
}