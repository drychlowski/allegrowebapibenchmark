﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoAddPackageInfoToPostBuyFormRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doAddPackageInfoToPostBuyFormRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public PackageInfoStruct[] packageInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long transactionId;

        public doAddPackageInfoToPostBuyFormRequest()
        {
        }

        public doAddPackageInfoToPostBuyFormRequest(string sessionId, long transactionId,
            PackageInfoStruct[] packageInfo)
        {
            this.sessionId = sessionId;
            this.transactionId = transactionId;
            this.packageInfo = packageInfo;
        }
    }
}