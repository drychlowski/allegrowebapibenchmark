﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetArchiveRefundsListResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetArchiveRefundsListResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int refundsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public ArchiveRefundsListTypeStruct[] refundsList;

        public doGetArchiveRefundsListResponse()
        {
        }

        public doGetArchiveRefundsListResponse(int refundsCount, ArchiveRefundsListTypeStruct[] refundsList)
        {
            this.refundsCount = refundsCount;
            this.refundsList = refundsList;
        }
    }
}