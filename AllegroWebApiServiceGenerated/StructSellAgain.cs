﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class StructSellAgain
    {
        /// <remarks />
        public long sellItemId { get; set; }

        /// <remarks />
        public string sellItemInfo { get; set; }

        /// <remarks />
        public int sellItemLocalId { get; set; }
    }
}