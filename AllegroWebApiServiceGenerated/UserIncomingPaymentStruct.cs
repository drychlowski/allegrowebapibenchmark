﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserIncomingPaymentStruct
    {
        /// <remarks />
        public long payTransId { get; set; }

        /// <remarks />
        public long payTransItId { get; set; }

        /// <remarks />
        public int payTransBuyerId { get; set; }

        /// <remarks />
        public string payTransType { get; set; }

        /// <remarks />
        public string payTransStatus { get; set; }

        /// <remarks />
        public float payTransAmount { get; set; }

        /// <remarks />
        public long payTransCreateDate { get; set; }

        /// <remarks />
        public long payTransRecvDate { get; set; }

        /// <remarks />
        public float payTransPrice { get; set; }

        /// <remarks />
        public int payTransCount { get; set; }

        /// <remarks />
        public float payTransPostageAmount { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PaymentDetailsStruct[] payTransDetails { get; set; }

        /// <remarks />
        public int payTransIncomplete { get; set; }

        /// <remarks />
        public long payTransMainId { get; set; }
    }
}