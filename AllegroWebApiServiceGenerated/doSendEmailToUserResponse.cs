﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doSendEmailToUserResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSendEmailToUserResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public long mailToUserReceiverId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string mailToUserResult;

        public doSendEmailToUserResponse()
        {
        }

        public doSendEmailToUserResponse(long mailToUserReceiverId, string mailToUserResult)
        {
            this.mailToUserReceiverId = mailToUserReceiverId;
            this.mailToUserResult = mailToUserResult;
        }
    }
}