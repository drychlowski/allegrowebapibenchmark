﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FiltersListType
    {
        /// <remarks />
        public string filterId { get; set; }

        /// <remarks />
        public string filterName { get; set; }

        /// <remarks />
        public string filterType { get; set; }

        /// <remarks />
        public string filterControlType { get; set; }

        /// <remarks />
        public string filterDataType { get; set; }

        /// <remarks />
        public bool filterIsRange { get; set; }

        /// <remarks />
        public int filterArraySize { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterArraySizeSpecified { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public FilterValueType[] filterValues { get; set; }

        /// <remarks />
        public FilterRelationType filterRelations { get; set; }
    }
}