﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormSellersStruct
    {
        /// <remarks />
        public int postBuyFormSellerId { get; set; }

        /// <remarks />
        public string postBuyFormSellerName { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormItemStruct[] postBuyFormItems { get; set; }

        /// <remarks />
        public int postBuyFormShipmentId { get; set; }

        /// <remarks />
        public float postBuyFormPostageAmount { get; set; }

        /// <remarks />
        public string postBuyFormMsgToSeller { get; set; }

        /// <remarks />
        public float postBuyFormAmount { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public long[] postBuyFormSurchargesList { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormShipmentTrackingStruct[] postBuyFormShipmentTracking { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormGdAddress { get; set; }

        /// <remarks />
        public string postBuyFormGdAdditionalInfo { get; set; }

        /// <remarks />
        public int postBuyFormSentBySeller { get; set; }
    }
}