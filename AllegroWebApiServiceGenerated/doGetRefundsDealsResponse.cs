﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetRefundsDealsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetRefundsDealsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int dealsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public RefundsDealsListType[] dealsList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public FiltersListType[] filtersList;

        public doGetRefundsDealsResponse()
        {
        }

        public doGetRefundsDealsResponse(int dealsCount, RefundsDealsListType[] dealsList,
            FiltersListType[] filtersList)
        {
            this.dealsCount = dealsCount;
            this.dealsList = dealsList;
            this.filtersList = filtersList;
        }
    }
}