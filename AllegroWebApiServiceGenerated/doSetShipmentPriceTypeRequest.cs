﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoSetShipmentPriceTypeRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSetShipmentPriceTypeRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int shipmentPriceTypeId;

        public doSetShipmentPriceTypeRequest()
        {
        }

        public doSetShipmentPriceTypeRequest(string sessionId, int shipmentPriceTypeId)
        {
            this.sessionId = sessionId;
            this.shipmentPriceTypeId = shipmentPriceTypeId;
        }
    }
}