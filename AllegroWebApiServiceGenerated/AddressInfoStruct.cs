﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class AddressInfoStruct
    {
        /// <remarks />
        public int addressType { get; set; }

        /// <remarks />
        public AddressUserDataStruct addressUserData { get; set; }
    }
}