﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetItemsListRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetItemsListRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int countryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public FilterOptionsType[] filterOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int resultOffset;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int resultScope;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int resultSize;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public SortOptionsType sortOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string webapiKey;

        public doGetItemsListRequest()
        {
        }

        public doGetItemsListRequest(string webapiKey, int countryId, FilterOptionsType[] filterOptions,
            SortOptionsType sortOptions, int resultSize, int resultOffset, int resultScope)
        {
            this.webapiKey = webapiKey;
            this.countryId = countryId;
            this.filterOptions = filterOptions;
            this.sortOptions = sortOptions;
            this.resultSize = resultSize;
            this.resultOffset = resultOffset;
            this.resultScope = resultScope;
        }
    }
}