﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoChangePriceItemRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doChangePriceItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public float newAdvertisementPrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public float newBuyNowPrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public float newReservePrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public float newStartingPrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doChangePriceItemRequest()
        {
        }

        public doChangePriceItemRequest(string sessionHandle, long itemId, float newStartingPrice,
            float newReservePrice, float newBuyNowPrice, float newAdvertisementPrice)
        {
            this.sessionHandle = sessionHandle;
            this.itemId = itemId;
            this.newStartingPrice = newStartingPrice;
            this.newReservePrice = newReservePrice;
            this.newBuyNowPrice = newBuyNowPrice;
            this.newAdvertisementPrice = newAdvertisementPrice;
        }
    }
}