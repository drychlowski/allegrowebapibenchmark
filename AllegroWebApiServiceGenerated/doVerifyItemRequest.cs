﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoVerifyItemRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doVerifyItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int localId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doVerifyItemRequest()
        {
        }

        public doVerifyItemRequest(string sessionHandle, int localId)
        {
            this.sessionHandle = sessionHandle;
            this.localId = localId;
        }
    }
}