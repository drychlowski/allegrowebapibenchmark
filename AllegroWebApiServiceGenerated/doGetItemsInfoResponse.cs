﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetItemsInfoResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    [XmlRoot("doGetItemsInfoResponse", Namespace = "https://webapi.allegro.pl/service.php")]
    public class doGetItemsInfoResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public ItemInfoStruct[] arrayItemListInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] arrayItemsAdminKilled;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] arrayItemsNotFound;

        public doGetItemsInfoResponse()
        {
        }

        public doGetItemsInfoResponse(ItemInfoStruct[] arrayItemListInfo, long[] arrayItemsNotFound,
            long[] arrayItemsAdminKilled)
        {
            this.arrayItemListInfo = arrayItemListInfo;
            this.arrayItemsNotFound = arrayItemsNotFound;
            this.arrayItemsAdminKilled = arrayItemsAdminKilled;
        }
    }
}