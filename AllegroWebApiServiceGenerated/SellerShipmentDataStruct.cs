﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SellerShipmentDataStruct
    {
        /// <remarks />
        public int sellerId { get; set; }

        /// <remarks />
        public SellerPaymentInfoStruct sellerPaymentInfo { get; set; }

        /// <remarks />
        public int sellerOtherShipmentIsActive { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ShipmentPaymentInfoStruct[] generalDeliveryPaymentInfo { get; set; }
    }
}