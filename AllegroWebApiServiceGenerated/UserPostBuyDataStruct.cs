﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserPostBuyDataStruct
    {
        /// <remarks />
        public UserDataStruct userData { get; set; }

        /// <remarks />
        public UserSentToDataStruct userSentToData { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] userBankAccounts { get; set; }

        /// <remarks />
        public CompanySecondAddressStruct companySecondAddress { get; set; }
    }
}