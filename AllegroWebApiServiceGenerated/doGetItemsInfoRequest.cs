﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetItemsInfoRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetItemsInfoRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 10)]
        public int getAdditionalServicesGroup;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 8)]
        public int getAfterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int getAttribs;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int getCompanyInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int getDesc;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 9)]
        public int getEan;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int getImageUrl;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int getPostageOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public int getProductInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] itemsIdArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doGetItemsInfoRequest()
        {
        }

        public doGetItemsInfoRequest(string sessionHandle, long[] itemsIdArray, int getDesc, int getImageUrl,
            int getAttribs, int getPostageOptions, int getCompanyInfo, int getProductInfo,
            int getAfterSalesServiceConditions, int getEan, int getAdditionalServicesGroup)
        {
            this.sessionHandle = sessionHandle;
            this.itemsIdArray = itemsIdArray;
            this.getDesc = getDesc;
            this.getImageUrl = getImageUrl;
            this.getAttribs = getAttribs;
            this.getPostageOptions = getPostageOptions;
            this.getCompanyInfo = getCompanyInfo;
            this.getProductInfo = getProductInfo;
            this.getAfterSalesServiceConditions = getAfterSalesServiceConditions;
            this.getEan = getEan;
            this.getAdditionalServicesGroup = getAdditionalServicesGroup;
        }
    }
}