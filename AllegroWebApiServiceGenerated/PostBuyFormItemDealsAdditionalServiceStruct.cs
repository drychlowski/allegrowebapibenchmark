﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormItemDealsAdditionalServiceStruct
    {
        /// <remarks />
        public string additionalServiceDefinitionId { get; set; }

        /// <remarks />
        public string additionalServiceName { get; set; }

        /// <remarks />
        public int additionalServiceQuantity { get; set; }

        /// <remarks />
        public float additionalServicePrice { get; set; }
    }
}