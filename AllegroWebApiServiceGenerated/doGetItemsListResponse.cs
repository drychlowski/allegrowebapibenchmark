﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetItemsListResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetItemsListResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public CategoriesListType categoriesList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        [XmlArrayItem("item", IsNullable = false)]
        public FiltersListType[] filtersList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        [XmlArrayItem("item", IsNullable = false)]
        public string[] filtersRejected;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int itemsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int itemsFeaturedCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public ItemsListType[] itemsList;

        public doGetItemsListResponse()
        {
        }

        public doGetItemsListResponse(int itemsCount, int itemsFeaturedCount, ItemsListType[] itemsList,
            CategoriesListType categoriesList, FiltersListType[] filtersList, string[] filtersRejected)
        {
            this.itemsCount = itemsCount;
            this.itemsFeaturedCount = itemsFeaturedCount;
            this.itemsList = itemsList;
            this.categoriesList = categoriesList;
            this.filtersList = filtersList;
            this.filtersRejected = filtersRejected;
        }
    }
}