﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SortOptionsStruct
    {
        /// <remarks />
        public int sortType { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool sortTypeSpecified { get; set; }

        /// <remarks />
        public int sortOrder { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool sortOrderSpecified { get; set; }
    }
}