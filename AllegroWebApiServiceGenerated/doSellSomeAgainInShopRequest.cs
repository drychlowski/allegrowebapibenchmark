﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoSellSomeAgainInShopRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSellSomeAgainInShopRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] localIds;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] sellItemsArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int sellProlongOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public long sellShopCategory;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int sellShopDuration;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int sellShopOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long sellStartingTime;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doSellSomeAgainInShopRequest()
        {
        }

        public doSellSomeAgainInShopRequest(string sessionHandle, long[] sellItemsArray, long sellStartingTime,
            int sellShopDuration, int sellShopOptions, int sellProlongOptions, long sellShopCategory, int[] localIds)
        {
            this.sessionHandle = sessionHandle;
            this.sellItemsArray = sellItemsArray;
            this.sellStartingTime = sellStartingTime;
            this.sellShopDuration = sellShopDuration;
            this.sellShopOptions = sellShopOptions;
            this.sellProlongOptions = sellProlongOptions;
            this.sellShopCategory = sellShopCategory;
            this.localIds = localIds;
        }
    }
}