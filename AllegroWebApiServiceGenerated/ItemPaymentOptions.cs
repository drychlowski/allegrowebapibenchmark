﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ItemPaymentOptions
    {
        /// <remarks />
        public int payOptionTransfer { get; set; }

        /// <remarks />
        public int payOptionOnDelivery { get; set; }

        /// <remarks />
        public int payOptionAllegroPay { get; set; }

        /// <remarks />
        public int payOptionSeeDesc { get; set; }

        /// <remarks />
        public int payOptionPayu { get; set; }

        /// <remarks />
        public int payOptionEscrow { get; set; }

        /// <remarks />
        public int payOptionQiwi { get; set; }
    }
}