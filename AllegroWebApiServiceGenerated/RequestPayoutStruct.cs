﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class RequestPayoutStruct
    {
        /// <remarks />
        public long payoutId { get; set; }

        /// <remarks />
        public float payoutAmount { get; set; }

        /// <remarks />
        public long payoutTime { get; set; }
    }
}