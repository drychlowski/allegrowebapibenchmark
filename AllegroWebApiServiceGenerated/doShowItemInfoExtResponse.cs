﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doShowItemInfoExtResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doShowItemInfoExtResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 10)]
        public string itemAdditionalServicesGroup;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 9)]
        public AfterSalesServiceConditionsStruct itemAfterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        [XmlArrayItem("item", IsNullable = false)]
        public AttribStruct[] itemAttribList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public ItemCatList[] itemCatPath;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public CompanyInfoStruct itemCompanyInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public ItemImageList[] itemImgList;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public ItemInfoExt itemListInfoExt;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public ItemPaymentOptions itemPaymentOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        [XmlArrayItem("item", IsNullable = false)]
        public PostageStruct[] itemPostageOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public ProductStruct itemProductInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 8)]
        public string itemVariants;

        public doShowItemInfoExtResponse()
        {
        }

        public doShowItemInfoExtResponse(ItemInfoExt itemListInfoExt, ItemCatList[] itemCatPath,
            ItemImageList[] itemImgList, AttribStruct[] itemAttribList, PostageStruct[] itemPostageOptions,
            ItemPaymentOptions itemPaymentOptions, CompanyInfoStruct itemCompanyInfo, ProductStruct itemProductInfo,
            string itemVariants, AfterSalesServiceConditionsStruct itemAfterSalesServiceConditions,
            string itemAdditionalServicesGroup)
        {
            this.itemListInfoExt = itemListInfoExt;
            this.itemCatPath = itemCatPath;
            this.itemImgList = itemImgList;
            this.itemAttribList = itemAttribList;
            this.itemPostageOptions = itemPostageOptions;
            this.itemPaymentOptions = itemPaymentOptions;
            this.itemCompanyInfo = itemCompanyInfo;
            this.itemProductInfo = itemProductInfo;
            this.itemVariants = itemVariants;
            this.itemAfterSalesServiceConditions = itemAfterSalesServiceConditions;
            this.itemAdditionalServicesGroup = itemAdditionalServicesGroup;
        }
    }
}