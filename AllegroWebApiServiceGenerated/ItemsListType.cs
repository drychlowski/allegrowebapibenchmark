﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ItemsListType
    {
        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public string itemTitle { get; set; }

        /// <remarks />
        public int leftCount { get; set; }

        /// <remarks />
        public int bidsCount { get; set; }

        /// <remarks />
        public int biddersCount { get; set; }

        /// <remarks />
        public string quantityType { get; set; }

        /// <remarks />
        public DateTime endingTime { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool endingTimeSpecified { get; set; }

        /// <remarks />
        public string timeToEnd { get; set; }

        /// <remarks />
        public int categoryId { get; set; }

        /// <remarks />
        public string conditionInfo { get; set; }

        /// <remarks />
        public int promotionInfo { get; set; }

        /// <remarks />
        public int additionalInfo { get; set; }

        /// <remarks />
        public UserInfoType sellerInfo { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PriceInfoType[] priceInfo { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PhotoInfoType[] photosInfo { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ParameterInfoType[] parametersInfo { get; set; }

        /// <remarks />
        public AdvertInfoType advertInfo { get; set; }
    }
}