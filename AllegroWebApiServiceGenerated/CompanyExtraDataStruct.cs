﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CompanyExtraDataStruct
    {
        /// <remarks />
        public string companyType { get; set; }

        /// <remarks />
        public string companyNip { get; set; }

        /// <remarks />
        public string companyRegon { get; set; }

        /// <remarks />
        public string companyKrs { get; set; }

        /// <remarks />
        public string companyActivitySort { get; set; }
    }
}