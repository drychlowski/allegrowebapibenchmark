﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetUserLoginResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetUserLoginResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string userLogin;

        public doGetUserLoginResponse()
        {
        }

        public doGetUserLoginResponse(string userLogin)
        {
            this.userLogin = userLogin;
        }
    }
}