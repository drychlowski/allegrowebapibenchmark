﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doSendPostBuyFormResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSendPostBuyFormResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public PostBuyFormStruct postBuyForm;

        public doSendPostBuyFormResponse()
        {
        }

        public doSendPostBuyFormResponse(PostBuyFormStruct postBuyForm)
        {
            this.postBuyForm = postBuyForm;
        }
    }
}