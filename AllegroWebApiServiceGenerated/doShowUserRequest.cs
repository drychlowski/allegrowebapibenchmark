﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoShowUserRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doShowUserRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int countryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long userId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public string userLogin;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string webapiKey;

        public doShowUserRequest()
        {
        }

        public doShowUserRequest(string webapiKey, int countryId, long userId, string userLogin)
        {
            this.webapiKey = webapiKey;
            this.countryId = countryId;
            this.userId = userId;
            this.userLogin = userLogin;
        }
    }
}