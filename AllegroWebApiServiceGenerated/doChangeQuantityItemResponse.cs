﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doChangeQuantityItemResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doChangeQuantityItemResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string itemInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int itemQuantityLeft;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int itemQuantitySold;

        public doChangeQuantityItemResponse()
        {
        }

        public doChangeQuantityItemResponse(long itemId, string itemInfo, int itemQuantityLeft, int itemQuantitySold)
        {
            this.itemId = itemId;
            this.itemInfo = itemInfo;
            this.itemQuantityLeft = itemQuantityLeft;
            this.itemQuantitySold = itemQuantitySold;
        }
    }
}