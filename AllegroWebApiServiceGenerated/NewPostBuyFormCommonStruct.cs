﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class NewPostBuyFormCommonStruct
    {
        /// <remarks />
        public string paymentMethodId { get; set; }

        /// <remarks />
        public int shipmentAddressType { get; set; }

        /// <remarks />
        public AddressUserDataStruct shipmentAddressData { get; set; }

        /// <remarks />
        public string contactPhone { get; set; }

        /// <remarks />
        public int invoiceOption { get; set; }

        /// <remarks />
        public InvoiceInfoStruct invoiceInfo { get; set; }
    }
}