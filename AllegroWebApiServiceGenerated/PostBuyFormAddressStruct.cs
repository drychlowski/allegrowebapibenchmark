﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormAddressStruct
    {
        /// <remarks />
        public int postBuyFormAdrCountry { get; set; }

        /// <remarks />
        public string postBuyFormAdrStreet { get; set; }

        /// <remarks />
        public string postBuyFormAdrPostcode { get; set; }

        /// <remarks />
        public string postBuyFormAdrCity { get; set; }

        /// <remarks />
        public string postBuyFormAdrFullName { get; set; }

        /// <remarks />
        public string postBuyFormAdrCompany { get; set; }

        /// <remarks />
        public string postBuyFormAdrPhone { get; set; }

        /// <remarks />
        public string postBuyFormAdrNip { get; set; }

        /// <remarks />
        public string postBuyFormCreatedDate { get; set; }

        /// <remarks />
        public int postBuyFormAdrType { get; set; }

        /// <remarks />
        public string postBuyFormAdrId { get; set; }
    }
}