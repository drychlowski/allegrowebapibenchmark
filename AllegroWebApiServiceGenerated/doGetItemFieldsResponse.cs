﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetItemFieldsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetItemFieldsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string additionalServicesGroup;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public AfterSalesServiceConditionsStruct afterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public FieldsValue[] itemFields;

        public doGetItemFieldsResponse()
        {
        }

        public doGetItemFieldsResponse(FieldsValue[] itemFields,
            AfterSalesServiceConditionsStruct afterSalesServiceConditions, string additionalServicesGroup)
        {
            this.itemFields = itemFields;
            this.afterSalesServiceConditions = afterSalesServiceConditions;
            this.additionalServicesGroup = additionalServicesGroup;
        }
    }
}