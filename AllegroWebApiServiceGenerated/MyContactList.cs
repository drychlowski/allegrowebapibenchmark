﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class MyContactList
    {
        /// <remarks />
        public int contactUserId { get; set; }

        /// <remarks />
        public string contactNick { get; set; }

        /// <remarks />
        public string contactFirstName { get; set; }

        /// <remarks />
        public string contactLastName { get; set; }

        /// <remarks />
        public string contactCompany { get; set; }

        /// <remarks />
        public string contactEmail { get; set; }

        /// <remarks />
        public string contactStreet { get; set; }

        /// <remarks />
        public string contactPostcode { get; set; }

        /// <remarks />
        public string contactCity { get; set; }

        /// <remarks />
        public string contactCountry { get; set; }

        /// <remarks />
        public string contactPhone { get; set; }

        /// <remarks />
        public string contactPhone2 { get; set; }

        /// <remarks />
        public string contactRating { get; set; }

        /// <remarks />
        public string contactBlocked { get; set; }
    }
}