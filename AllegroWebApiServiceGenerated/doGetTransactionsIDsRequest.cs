﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetTransactionsIDsRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetTransactionsIDsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] itemsIdArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] shipmentIdArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string userRole;

        public doGetTransactionsIDsRequest()
        {
        }

        public doGetTransactionsIDsRequest(string sessionHandle, long[] itemsIdArray, string userRole,
            long[] shipmentIdArray)
        {
            this.sessionHandle = sessionHandle;
            this.itemsIdArray = itemsIdArray;
            this.userRole = userRole;
            this.shipmentIdArray = shipmentIdArray;
        }
    }
}