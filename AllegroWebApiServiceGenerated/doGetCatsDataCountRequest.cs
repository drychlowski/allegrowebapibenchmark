﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetCatsDataCountRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetCatsDataCountRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int countryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long localVersion;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public bool onlyLeaf;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string webapiKey;

        public doGetCatsDataCountRequest()
        {
        }

        public doGetCatsDataCountRequest(int countryId, long localVersion, string webapiKey, bool onlyLeaf)
        {
            this.countryId = countryId;
            this.localVersion = localVersion;
            this.webapiKey = webapiKey;
            this.onlyLeaf = onlyLeaf;
        }
    }
}