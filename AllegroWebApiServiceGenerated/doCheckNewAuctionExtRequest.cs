﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoCheckNewAuctionExtRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doCheckNewAuctionExtRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public AfterSalesServiceConditionsStruct afterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public FieldsValue[] fields;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        [XmlArrayItem("item", IsNullable = false)]
        public TagNameStruct[] tags;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public VariantStruct[] variants;

        public doCheckNewAuctionExtRequest()
        {
        }

        public doCheckNewAuctionExtRequest(string sessionHandle, FieldsValue[] fields, VariantStruct[] variants,
            TagNameStruct[] tags, AfterSalesServiceConditionsStruct afterSalesServiceConditions)
        {
            this.sessionHandle = sessionHandle;
            this.fields = fields;
            this.variants = variants;
            this.tags = tags;
            this.afterSalesServiceConditions = afterSalesServiceConditions;
        }
    }
}