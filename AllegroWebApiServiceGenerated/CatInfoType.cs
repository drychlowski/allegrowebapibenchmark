﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CatInfoType
    {
        /// <remarks />
        public int catId { get; set; }

        /// <remarks />
        public string catName { get; set; }

        /// <remarks />
        public int catParent { get; set; }

        /// <remarks />
        public int catPosition { get; set; }

        /// <remarks />
        public int catIsProductCatalogueEnabled { get; set; }

        /// <remarks />
        public bool catIsLeaf { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool catIsLeafSpecified { get; set; }
    }
}