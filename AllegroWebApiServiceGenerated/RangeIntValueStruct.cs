﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class RangeIntValueStruct
    {
        /// <remarks />
        public int fvalueRangeIntMin { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool fvalueRangeIntMinSpecified { get; set; }

        /// <remarks />
        public int fvalueRangeIntMax { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool fvalueRangeIntMaxSpecified { get; set; }
    }
}