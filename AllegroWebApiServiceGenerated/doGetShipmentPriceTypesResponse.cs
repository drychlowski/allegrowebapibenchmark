﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetShipmentPriceTypesResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetShipmentPriceTypesResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public ShipmentPriceTypeStruct[] shipmentPriceTypes;

        public doGetShipmentPriceTypesResponse()
        {
        }

        public doGetShipmentPriceTypesResponse(ShipmentPriceTypeStruct[] shipmentPriceTypes)
        {
            this.shipmentPriceTypes = shipmentPriceTypes;
        }
    }
}