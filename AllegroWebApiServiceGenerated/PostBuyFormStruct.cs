﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormStruct
    {
        /// <remarks />
        public long transactionId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public long[] transactionPackageIds { get; set; }

        /// <remarks />
        public TransactionPayByLinkStruct transactionPayByLink { get; set; }
    }
}