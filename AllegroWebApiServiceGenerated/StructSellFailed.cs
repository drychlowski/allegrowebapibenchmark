﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class StructSellFailed
    {
        /// <remarks />
        public long sellItemId { get; set; }

        /// <remarks />
        public string sellFaultCode { get; set; }

        /// <remarks />
        public string sellFaultString { get; set; }
    }
}