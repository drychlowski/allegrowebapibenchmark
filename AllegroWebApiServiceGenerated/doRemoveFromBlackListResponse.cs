﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doRemoveFromBlackListResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doRemoveFromBlackListResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public BlackListResStruct[] blackListResult;

        public doRemoveFromBlackListResponse()
        {
        }

        public doRemoveFromBlackListResponse(BlackListResStruct[] blackListResult)
        {
            this.blackListResult = blackListResult;
        }
    }
}