﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ItemInfoStruct
    {
        /// <remarks />
        public ItemInfo itemInfo { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ItemCatList[] itemCats { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ItemImageList[] itemImages { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public AttribStruct[] itemAttribs { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostageStruct[] itemPostageOptions { get; set; }

        /// <remarks />
        public ItemPaymentOptions itemPaymentOptions { get; set; }

        /// <remarks />
        public CompanyInfoStruct itemCompanyInfo { get; set; }

        /// <remarks />
        public ProductStruct itemProductInfo { get; set; }

        /// <remarks />
        public AfterSalesServiceConditionsStruct itemAfterSalesServiceConditions { get; set; }

        /// <remarks />
        public string itemAdditionalServicesGroup { get; set; }
    }
}