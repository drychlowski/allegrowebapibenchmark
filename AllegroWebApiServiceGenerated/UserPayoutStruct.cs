﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserPayoutStruct
    {
        /// <remarks />
        public long payTransId { get; set; }

        /// <remarks />
        public string payTransStatus { get; set; }

        /// <remarks />
        public float payTransAmount { get; set; }

        /// <remarks />
        public long payTransCreateDate { get; set; }

        /// <remarks />
        public long payTransRecvDate { get; set; }

        /// <remarks />
        public long payTransCancelDate { get; set; }

        /// <remarks />
        public string payTransReport { get; set; }
    }
}