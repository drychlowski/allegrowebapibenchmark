﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doFinishItemsResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doFinishItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public FinishFailureStruct[] finishItemsFailed;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] finishItemsSucceed;

        public doFinishItemsResponse()
        {
        }

        public doFinishItemsResponse(long[] finishItemsSucceed, FinishFailureStruct[] finishItemsFailed)
        {
            this.finishItemsSucceed = finishItemsSucceed;
            this.finishItemsFailed = finishItemsFailed;
        }
    }
}