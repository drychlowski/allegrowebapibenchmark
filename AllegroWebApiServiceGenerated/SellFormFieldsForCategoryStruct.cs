﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SellFormFieldsForCategoryStruct
    {
        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public SellFormType[] sellFormFieldsList { get; set; }

        /// <remarks />
        public long sellFormFieldsVersionKey { get; set; }

        /// <remarks />
        public string sellFormFieldsComponentValue { get; set; }
    }
}