﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetRefundsReasonsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetRefundsReasonsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int reasonsCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public ReasonInfoType[] reasonsList;

        public doGetRefundsReasonsResponse()
        {
        }

        public doGetRefundsReasonsResponse(int reasonsCount, ReasonInfoType[] reasonsList)
        {
            this.reasonsCount = reasonsCount;
            this.reasonsList = reasonsList;
        }
    }
}