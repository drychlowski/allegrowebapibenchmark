﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class AlcoholDataStruct
    {
        /// <remarks />
        public string alcoholOpeningDate { get; set; }

        /// <remarks />
        public string alcoholExpirationDate { get; set; }

        /// <remarks />
        public string alcoholShopName { get; set; }

        /// <remarks />
        public string alcoholShopAddress { get; set; }

        /// <remarks />
        public string alcoholShopPostcode { get; set; }

        /// <remarks />
        public string alcoholShopCity { get; set; }

        /// <remarks />
        public string alcoholShopCommune { get; set; }

        /// <remarks />
        public int alcoholShopCountryId { get; set; }

        /// <remarks />
        public int alcoholShopStateId { get; set; }

        /// <remarks />
        public string alcoholPermitNumber { get; set; }

        /// <remarks />
        public string alcoholPermitAuthority { get; set; }

        /// <remarks />
        public string alcoholPermitInfo { get; set; }
    }
}