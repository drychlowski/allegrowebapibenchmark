﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PayoutAutoFrequencyStruct
    {
        /// <remarks />
        public short frequencyType { get; set; }

        /// <remarks />
        public short frequencyWeekValue { get; set; }

        /// <remarks />
        public short frequencyMonthValue { get; set; }
    }
}