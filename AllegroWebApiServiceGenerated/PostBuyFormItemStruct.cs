﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormItemStruct
    {
        /// <remarks />
        public int postBuyFormItQuantity { get; set; }

        /// <remarks />
        public float postBuyFormItAmount { get; set; }

        /// <remarks />
        public long postBuyFormItId { get; set; }

        /// <remarks />
        public string postBuyFormItTitle { get; set; }

        /// <remarks />
        public int postBuyFormItCountry { get; set; }

        /// <remarks />
        public float postBuyFormItPrice { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormItemDealsStruct[] postBuyFormItDeals { get; set; }
    }
}