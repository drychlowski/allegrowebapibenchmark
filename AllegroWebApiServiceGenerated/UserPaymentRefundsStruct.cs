﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserPaymentRefundsStruct
    {
        /// <remarks />
        public long payRefundTransId { get; set; }

        /// <remarks />
        public long payRefundItId { get; set; }

        /// <remarks />
        public int payRefundSellerId { get; set; }

        /// <remarks />
        public float payRefundValue { get; set; }

        /// <remarks />
        public string payRefundReason { get; set; }

        /// <remarks />
        public long payRefundDate { get; set; }
    }
}