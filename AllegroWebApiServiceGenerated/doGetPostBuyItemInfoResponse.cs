﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetPostBuyItemInfoResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetPostBuyItemInfoResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyItemInfoStruct[] itemPostBuyFormInfo;

        public doGetPostBuyItemInfoResponse()
        {
        }

        public doGetPostBuyItemInfoResponse(PostBuyItemInfoStruct[] itemPostBuyFormInfo)
        {
            this.itemPostBuyFormInfo = itemPostBuyFormInfo;
        }
    }
}