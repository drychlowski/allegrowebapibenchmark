﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetSellFormFieldsForCategoryResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetSellFormFieldsForCategoryResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public SellFormFieldsForCategoryStruct sellFormFieldsForCategory;

        public doGetSellFormFieldsForCategoryResponse()
        {
        }

        public doGetSellFormFieldsForCategoryResponse(SellFormFieldsForCategoryStruct sellFormFieldsForCategory)
        {
            this.sellFormFieldsForCategory = sellFormFieldsForCategory;
        }
    }
}