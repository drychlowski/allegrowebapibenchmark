﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class RefundsDealsListType
    {
        /// <remarks />
        public long dealId { get; set; }

        /// <remarks />
        public DateTime dealDate { get; set; }

        /// <remarks />
        public int timeLeft { get; set; }

        /// <remarks />
        public int buyerId { get; set; }

        /// <remarks />
        public string buyerLogin { get; set; }

        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public string itemTitle { get; set; }

        /// <remarks />
        public int bidsCount { get; set; }

        /// <remarks />
        public string quantityType { get; set; }

        /// <remarks />
        public string paymentStatus { get; set; }
    }
}