﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FilterValueType
    {
        /// <remarks />
        public string filterValueId { get; set; }

        /// <remarks />
        public string filterValueName { get; set; }

        /// <remarks />
        public int filterValueCount { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterValueCountSpecified { get; set; }
    }
}