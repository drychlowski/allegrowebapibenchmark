﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ReasonInfoType
    {
        /// <remarks />
        public int reasonId { get; set; }

        /// <remarks />
        public string reasonName { get; set; }

        /// <remarks />
        public int maxQuantity { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool maxQuantitySpecified { get; set; }
    }
}