﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PaymentsInfoStruct
    {
        /// <remarks />
        public float paymentsBalance { get; set; }

        /// <remarks />
        public string paymentsBankAccount { get; set; }

        /// <remarks />
        public PaymentsUserDataStruct paymentsUserData { get; set; }

        /// <remarks />
        public PaymentsPayoutStruct paymentsPayout { get; set; }

        /// <remarks />
        public int paymentsNotifications { get; set; }
    }
}