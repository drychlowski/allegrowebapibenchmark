﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ProductStruct
    {
        /// <remarks />
        public long productId { get; set; }

        /// <remarks />
        public string productName { get; set; }

        /// <remarks />
        public string productDescription { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] productImagesList { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ProductParametersGroupStruct[] productParametersGroupList { get; set; }
    }
}