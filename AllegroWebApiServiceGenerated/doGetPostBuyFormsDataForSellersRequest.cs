﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetPostBuyFormsDataForSellersRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetPostBuyFormsDataForSellersRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] transactionsIdsArray;

        public doGetPostBuyFormsDataForSellersRequest()
        {
        }

        public doGetPostBuyFormsDataForSellersRequest(string sessionId, long[] transactionsIdsArray)
        {
            this.sessionId = sessionId;
            this.transactionsIdsArray = transactionsIdsArray;
        }
    }
}