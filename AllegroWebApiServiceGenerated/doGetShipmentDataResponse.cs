﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetShipmentDataResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetShipmentDataResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long localVersion;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public ShipmentDataStruct[] shipmentDataList;

        public doGetShipmentDataResponse()
        {
        }

        public doGetShipmentDataResponse(ShipmentDataStruct[] shipmentDataList, long localVersion)
        {
            this.shipmentDataList = shipmentDataList;
            this.localVersion = localVersion;
        }
    }
}