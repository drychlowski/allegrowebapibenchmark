﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class RelatedItemStruct
    {
        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public string itemTitle { get; set; }

        /// <remarks />
        public string itemThumbnail { get; set; }

        /// <remarks />
        public float itemPrice { get; set; }

        /// <remarks />
        public int itemBoughtCount { get; set; }

        /// <remarks />
        public float itemAmount { get; set; }

        /// <remarks />
        public int itemPaymentType { get; set; }

        /// <remarks />
        public long itemSellerId { get; set; }

        /// <remarks />
        public string itemSellerName { get; set; }

        /// <remarks />
        public int itemInvoiceInfo { get; set; }

        /// <remarks />
        public int itemCategoryId { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool itemCategoryIdSpecified { get; set; }
    }
}