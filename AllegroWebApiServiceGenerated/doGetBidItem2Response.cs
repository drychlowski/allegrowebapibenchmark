﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetBidItem2Response", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    [XmlRoot("doGetBidItem2Response", Namespace = "https://webapi.allegro.pl/service.php")]
    public class doGetBidItem2Response
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public BidListStruct2[] biditemList;

        public doGetBidItem2Response()
        {
        }

        public doGetBidItem2Response(BidListStruct2[] biditemList)
        {
            this.biditemList = biditemList;
        }
    }
}