﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PayoutRefundToStruct
    {
        /// <remarks />
        public long tranasctionId { get; set; }

        /// <remarks />
        public long refundId { get; set; }

        /// <remarks />
        public string refundReason { get; set; }

        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public long toUserId { get; set; }

        /// <remarks />
        public float amount { get; set; }

        /// <remarks />
        public string paidDate { get; set; }
    }
}