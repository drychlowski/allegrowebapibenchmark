﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoNewAuctionExtRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doNewAuctionExtRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 8)]
        public string additionalServicesGroup;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public AfterSalesServiceConditionsStruct afterSalesServiceConditions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public FieldsValue[] fields;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public ItemTemplateCreateStruct itemTemplateCreate;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int itemTemplateId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int localId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        [XmlArrayItem("item", IsNullable = false)]
        public TagNameStruct[] tags;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        [XmlArrayItem("item", IsNullable = false)]
        public VariantStruct[] variants;

        public doNewAuctionExtRequest()
        {
        }

        public doNewAuctionExtRequest(string sessionHandle, FieldsValue[] fields, int itemTemplateId, int localId,
            ItemTemplateCreateStruct itemTemplateCreate, VariantStruct[] variants, TagNameStruct[] tags,
            AfterSalesServiceConditionsStruct afterSalesServiceConditions, string additionalServicesGroup)
        {
            this.sessionHandle = sessionHandle;
            this.fields = fields;
            this.itemTemplateId = itemTemplateId;
            this.localId = localId;
            this.itemTemplateCreate = itemTemplateCreate;
            this.variants = variants;
            this.tags = tags;
            this.afterSalesServiceConditions = afterSalesServiceConditions;
            this.additionalServicesGroup = additionalServicesGroup;
        }
    }
}