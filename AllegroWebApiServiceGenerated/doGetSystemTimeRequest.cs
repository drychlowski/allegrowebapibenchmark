﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetSystemTimeRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetSystemTimeRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int countryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string webapiKey;

        public doGetSystemTimeRequest()
        {
        }

        public doGetSystemTimeRequest(int countryId, string webapiKey)
        {
            this.countryId = countryId;
            this.webapiKey = webapiKey;
        }
    }
}