﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyDataResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetMyDataResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public AlcoholDataStruct alcoholData;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public CompanyExtraDataStruct companyExtraData;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public CompanySecondAddressStruct companySecondAddress;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public InvoiceDataStruct invoiceData;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public PharmacyDataStruct pharmacyData;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public RelatedPersonsStruct relatedPersons;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public UserDataStruct userData;

        public doGetMyDataResponse()
        {
        }

        public doGetMyDataResponse(UserDataStruct userData, InvoiceDataStruct invoiceData,
            CompanyExtraDataStruct companyExtraData, CompanySecondAddressStruct companySecondAddress,
            PharmacyDataStruct pharmacyData, AlcoholDataStruct alcoholData, RelatedPersonsStruct relatedPersons)
        {
            this.userData = userData;
            this.invoiceData = invoiceData;
            this.companyExtraData = companyExtraData;
            this.companySecondAddress = companySecondAddress;
            this.pharmacyData = pharmacyData;
            this.alcoholData = alcoholData;
            this.relatedPersons = relatedPersons;
        }
    }
}