﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetFilledPostBuyFormsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetFilledPostBuyFormsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public FilledPostBuyFormsStruct filledPostBuyForms;

        public doGetFilledPostBuyFormsResponse()
        {
        }

        public doGetFilledPostBuyFormsResponse(FilledPostBuyFormsStruct filledPostBuyForms)
        {
            this.filledPostBuyForms = filledPostBuyForms;
        }
    }
}