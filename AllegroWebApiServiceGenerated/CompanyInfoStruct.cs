﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CompanyInfoStruct
    {
        /// <remarks />
        public string companyFirstName { get; set; }

        /// <remarks />
        public string companyLastName { get; set; }

        /// <remarks />
        public string companyName { get; set; }

        /// <remarks />
        public string companyAddress { get; set; }

        /// <remarks />
        public string companyPostcode { get; set; }

        /// <remarks />
        public string companyCity { get; set; }

        /// <remarks />
        public string companyNip { get; set; }
    }
}