﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class BidItemStruct
    {
        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public string itemTitle { get; set; }

        /// <remarks />
        public string itemThumbnailUrl { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ItemPriceStruct[] itemPrice { get; set; }

        /// <remarks />
        public int itemBidQuantity { get; set; }

        /// <remarks />
        public int itemLeftQuantity { get; set; }

        /// <remarks />
        public int itemQuantityType { get; set; }

        /// <remarks />
        public long itemEndTime { get; set; }

        /// <remarks />
        public string itemEndTimeLeft { get; set; }

        /// <remarks />
        public UserInfoStruct itemSeller { get; set; }

        /// <remarks />
        public int itemBiddersCounter { get; set; }

        /// <remarks />
        public UserInfoStruct itemHighestBidder { get; set; }

        /// <remarks />
        public int itemCategoryId { get; set; }

        /// <remarks />
        public int itemViewsCounter { get; set; }

        /// <remarks />
        public string itemNote { get; set; }

        /// <remarks />
        public int itemSpecialInfo { get; set; }

        /// <remarks />
        public int itemShopInfo { get; set; }

        /// <remarks />
        public long itemProductInfo { get; set; }

        /// <remarks />
        public int itemPayuInfo { get; set; }
    }
}