﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ShipmentPaymentInfoStruct
    {
        /// <remarks />
        public int shipmentId { get; set; }

        /// <remarks />
        public string shipmentName { get; set; }

        /// <remarks />
        public float shipmentAmount { get; set; }

        /// <remarks />
        public int shipmentPaymentType { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public long[] shipmentItemIds { get; set; }
    }
}