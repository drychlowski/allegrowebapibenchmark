﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoSendPostBuyFormRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSendPostBuyFormRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public NewPostBuyFormCommonStruct newPostBuyFormCommon;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public NewPostBuyFormSellerStruct[] newPostBuyFormSeller;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        public doSendPostBuyFormRequest()
        {
        }

        public doSendPostBuyFormRequest(string sessionId, NewPostBuyFormSellerStruct[] newPostBuyFormSeller,
            NewPostBuyFormCommonStruct newPostBuyFormCommon)
        {
            this.sessionId = sessionId;
            this.newPostBuyFormSeller = newPostBuyFormSeller;
            this.newPostBuyFormCommon = newPostBuyFormCommon;
        }
    }
}