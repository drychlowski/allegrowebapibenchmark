﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CompanySecondAddressStruct
    {
        /// <remarks />
        public string companyWorkerFirstName { get; set; }

        /// <remarks />
        public string companyWorkerLastName { get; set; }

        /// <remarks />
        public string companyAddress { get; set; }

        /// <remarks />
        public string companyPostcode { get; set; }

        /// <remarks />
        public string companyCity { get; set; }

        /// <remarks />
        public int companyCountryId { get; set; }

        /// <remarks />
        public int companyStateId { get; set; }
    }
}