﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoFinishItemRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doFinishItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int finishCancelAllBids;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public string finishCancelReason;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long finishItemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doFinishItemRequest()
        {
        }

        public doFinishItemRequest(string sessionHandle, long finishItemId, int finishCancelAllBids,
            string finishCancelReason)
        {
            this.sessionHandle = sessionHandle;
            this.finishItemId = finishItemId;
            this.finishCancelAllBids = finishCancelAllBids;
            this.finishCancelReason = finishCancelReason;
        }
    }
}