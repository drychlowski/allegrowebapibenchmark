﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetFreeDeliveryAmountResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetFreeDeliveryAmountResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int activeFlag;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public float freeDeliveryAmount;

        public doGetFreeDeliveryAmountResponse()
        {
        }

        public doGetFreeDeliveryAmountResponse(float freeDeliveryAmount, int activeFlag)
        {
            this.freeDeliveryAmount = freeDeliveryAmount;
            this.activeFlag = activeFlag;
        }
    }
}