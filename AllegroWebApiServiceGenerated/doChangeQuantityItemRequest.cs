﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoChangeQuantityItemRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doChangeQuantityItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int newItemQuantity;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doChangeQuantityItemRequest()
        {
        }

        public doChangeQuantityItemRequest(string sessionHandle, long itemId, int newItemQuantity)
        {
            this.sessionHandle = sessionHandle;
            this.itemId = itemId;
            this.newItemQuantity = newItemQuantity;
        }
    }
}