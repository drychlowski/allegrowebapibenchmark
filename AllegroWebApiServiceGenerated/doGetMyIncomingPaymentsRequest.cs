﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetMyIncomingPaymentsRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyIncomingPaymentsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int buyerId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public int strictedSearch;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int transOffset;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int transPageLimit;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public long transRecvDateFrom;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long transRecvDateTo;

        public doGetMyIncomingPaymentsRequest()
        {
        }

        public doGetMyIncomingPaymentsRequest(string sessionHandle, int buyerId, long itemId, long transRecvDateFrom,
            long transRecvDateTo, int transPageLimit, int transOffset, int strictedSearch)
        {
            this.sessionHandle = sessionHandle;
            this.buyerId = buyerId;
            this.itemId = itemId;
            this.transRecvDateFrom = transRecvDateFrom;
            this.transRecvDateTo = transRecvDateTo;
            this.transPageLimit = transPageLimit;
            this.transOffset = transOffset;
            this.strictedSearch = strictedSearch;
        }
    }
}