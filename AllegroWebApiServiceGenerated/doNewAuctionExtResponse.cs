﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doNewAuctionExtResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doNewAuctionExtResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string itemInfo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int itemIsAllegroStandard;

        public doNewAuctionExtResponse()
        {
        }

        public doNewAuctionExtResponse(long itemId, string itemInfo, int itemIsAllegroStandard)
        {
            this.itemId = itemId;
            this.itemInfo = itemInfo;
            this.itemIsAllegroStandard = itemIsAllegroStandard;
        }
    }
}