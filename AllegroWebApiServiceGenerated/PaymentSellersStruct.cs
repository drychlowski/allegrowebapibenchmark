﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PaymentSellersStruct
    {
        /// <remarks />
        public int payTransSellerId { get; set; }

        /// <remarks />
        public string payTransSellerName { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PaymentItemsStruct[] payTransItems { get; set; }

        /// <remarks />
        public float payTransSellerPostageAmount { get; set; }
    }
}