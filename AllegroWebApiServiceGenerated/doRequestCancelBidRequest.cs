﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoRequestCancelBidRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doRequestCancelBidRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string requestCancelReason;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long requestItemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doRequestCancelBidRequest()
        {
        }

        public doRequestCancelBidRequest(string sessionHandle, long requestItemId, string requestCancelReason)
        {
            this.sessionHandle = sessionHandle;
            this.requestItemId = requestItemId;
            this.requestCancelReason = requestCancelReason;
        }
    }
}