﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostageStruct
    {
        /// <remarks />
        public float postageAmount { get; set; }

        /// <remarks />
        public float postageAmountAdd { get; set; }

        /// <remarks />
        public int postagePackSize { get; set; }

        /// <remarks />
        public int postageId { get; set; }

        /// <remarks />
        public int postageFreeShipping { get; set; }

        /// <remarks />
        public int postageFreeReturn { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool postageFreeReturnSpecified { get; set; }

        /// <remarks />
        public FulfillmentTimeStruct postageFulfillmentTime { get; set; }
    }
}