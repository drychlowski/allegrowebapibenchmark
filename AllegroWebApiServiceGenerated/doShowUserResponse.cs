﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doShowUserResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doShowUserResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 9)]
        public int userBlocked;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 8)]
        public int userClosed;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 19)]
        public int userCompanyIcon;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int userCountry;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public long userCreateDate;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 11)]
        public int userHasPage;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 18)]
        public int userHasShop;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public long userId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 22)]
        public int userIsAllegroStandard;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 23)]
        public int userIsB2cSeller;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 13)]
        public int userIsEco;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int userIsNewUser;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 12)]
        public int userIsSseller;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 17)]
        public int userJuniorStatus;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string userLogin;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long userLoginDate;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 15)]
        public ShowUserFeedbacks userNegativeFeedback;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 16)]
        public ShowUserFeedbacks userNeutralFeedback;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public int userNotActivated;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 14)]
        public ShowUserFeedbacks userPositiveFeedback;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int userRating;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 21)]
        [XmlArrayItem("item", IsNullable = false)]
        public SellRatingAverageStruct[] userSellRatingAverage;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 20)]
        public int userSellRatingCount;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 10)]
        public int userTerminated;

        public doShowUserResponse()
        {
        }

        public doShowUserResponse(
            long userId,
            string userLogin,
            int userCountry,
            long userCreateDate,
            long userLoginDate,
            int userRating,
            int userIsNewUser,
            int userNotActivated,
            int userClosed,
            int userBlocked,
            int userTerminated,
            int userHasPage,
            int userIsSseller,
            int userIsEco,
            ShowUserFeedbacks userPositiveFeedback,
            ShowUserFeedbacks userNegativeFeedback,
            ShowUserFeedbacks userNeutralFeedback,
            int userJuniorStatus,
            int userHasShop,
            int userCompanyIcon,
            int userSellRatingCount,
            SellRatingAverageStruct[] userSellRatingAverage,
            int userIsAllegroStandard,
            int userIsB2cSeller)
        {
            this.userId = userId;
            this.userLogin = userLogin;
            this.userCountry = userCountry;
            this.userCreateDate = userCreateDate;
            this.userLoginDate = userLoginDate;
            this.userRating = userRating;
            this.userIsNewUser = userIsNewUser;
            this.userNotActivated = userNotActivated;
            this.userClosed = userClosed;
            this.userBlocked = userBlocked;
            this.userTerminated = userTerminated;
            this.userHasPage = userHasPage;
            this.userIsSseller = userIsSseller;
            this.userIsEco = userIsEco;
            this.userPositiveFeedback = userPositiveFeedback;
            this.userNegativeFeedback = userNegativeFeedback;
            this.userNeutralFeedback = userNeutralFeedback;
            this.userJuniorStatus = userJuniorStatus;
            this.userHasShop = userHasShop;
            this.userCompanyIcon = userCompanyIcon;
            this.userSellRatingCount = userSellRatingCount;
            this.userSellRatingAverage = userSellRatingAverage;
            this.userIsAllegroStandard = userIsAllegroStandard;
            this.userIsB2cSeller = userIsB2cSeller;
        }
    }
}