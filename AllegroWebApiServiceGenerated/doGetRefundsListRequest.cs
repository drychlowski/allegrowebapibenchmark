﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetRefundsListRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetRefundsListRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public FilterOptionsType[] filterOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int resultOffset;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int resultSize;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        public doGetRefundsListRequest()
        {
        }

        public doGetRefundsListRequest(string sessionId, FilterOptionsType[] filterOptions, int resultSize,
            int resultOffset)
        {
            this.sessionId = sessionId;
            this.filterOptions = filterOptions;
            this.resultSize = resultSize;
            this.resultOffset = resultOffset;
        }
    }
}