﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CategoryTreeType
    {
        /// <remarks />
        public int categoryId { get; set; }

        /// <remarks />
        public string categoryName { get; set; }

        /// <remarks />
        public int categoryParentId { get; set; }

        /// <remarks />
        public int categoryItemsCount { get; set; }
    }
}