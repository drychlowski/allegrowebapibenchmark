﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doAddToBlackListResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doAddToBlackListResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public UserBlackListAddResultStruct[] userBlackListResultsArr;

        public doAddToBlackListResponse()
        {
        }

        public doAddToBlackListResponse(UserBlackListAddResultStruct[] userBlackListResultsArr)
        {
            this.userBlackListResultsArr = userBlackListResultsArr;
        }
    }
}