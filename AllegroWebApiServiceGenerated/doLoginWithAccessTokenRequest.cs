﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoLoginWithAccessTokenRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doLoginWithAccessTokenRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string accessToken;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int countryCode;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string webapiKey;

        public doLoginWithAccessTokenRequest()
        {
        }

        public doLoginWithAccessTokenRequest(string accessToken, int countryCode, string webapiKey)
        {
            this.accessToken = accessToken;
            this.countryCode = countryCode;
            this.webapiKey = webapiKey;
        }
    }
}