﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SiteJournalDealsStruct
    {
        /// <remarks />
        public long dealEventId { get; set; }

        /// <remarks />
        public int dealEventType { get; set; }

        /// <remarks />
        public long dealEventTime { get; set; }

        /// <remarks />
        public long dealId { get; set; }

        /// <remarks />
        public long dealTransactionId { get; set; }

        /// <remarks />
        public int dealSellerId { get; set; }

        /// <remarks />
        public long dealItemId { get; set; }

        /// <remarks />
        public int dealBuyerId { get; set; }

        /// <remarks />
        public int dealQuantity { get; set; }
    }
}