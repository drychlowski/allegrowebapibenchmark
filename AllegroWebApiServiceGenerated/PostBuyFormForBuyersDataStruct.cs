﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormForBuyersDataStruct
    {
        /// <remarks />
        public long postBuyFormId { get; set; }

        /// <remarks />
        public int postBuyFormBuyerId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormSellersStruct[] postBuyFormSellers { get; set; }

        /// <remarks />
        public float postBuyFormTotalAmount { get; set; }

        /// <remarks />
        public int postBuyFormInvoiceOption { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormInvoiceData { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormShipmentAddress { get; set; }

        /// <remarks />
        public string postBuyFormPayType { get; set; }

        /// <remarks />
        public long postBuyFormPayId { get; set; }

        /// <remarks />
        public string postBuyFormPayStatus { get; set; }

        /// <remarks />
        public string postBuyFormDateInit { get; set; }

        /// <remarks />
        public string postBuyFormDateRecv { get; set; }

        /// <remarks />
        public string postBuyFormDateCancel { get; set; }

        /// <remarks />
        public float postBuyFormPaymentAmount { get; set; }
    }
}