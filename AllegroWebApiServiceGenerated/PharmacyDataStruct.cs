﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PharmacyDataStruct
    {
        /// <remarks />
        public string pharmacyOpeningDate { get; set; }

        /// <remarks />
        public string pharmacyExpirationDate { get; set; }

        /// <remarks />
        public string pharmacyName { get; set; }

        /// <remarks />
        public string pharmacyPharmacistFullName { get; set; }

        /// <remarks />
        public string pharmacyAddress { get; set; }

        /// <remarks />
        public string pharmacyPostcode { get; set; }

        /// <remarks />
        public string pharmacyCity { get; set; }

        /// <remarks />
        public string pharmacyCommune { get; set; }

        /// <remarks />
        public int pharmacyCountryId { get; set; }

        /// <remarks />
        public int pharmacyStateId { get; set; }

        /// <remarks />
        public string pharmacyPermitNumber { get; set; }

        /// <remarks />
        public string pharmacyPermitInfo { get; set; }
    }
}