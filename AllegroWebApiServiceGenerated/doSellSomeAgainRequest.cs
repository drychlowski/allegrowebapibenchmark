﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoSellSomeAgainRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doSellSomeAgainRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] localIds;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int sellAuctionDuration;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] sellItemsArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int sellOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int sellProlongOptions;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long sellStartingTime;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doSellSomeAgainRequest()
        {
        }

        public doSellSomeAgainRequest(string sessionHandle, long[] sellItemsArray, long sellStartingTime,
            int sellAuctionDuration, int sellOptions, int[] localIds, int sellProlongOptions)
        {
            this.sessionHandle = sessionHandle;
            this.sellItemsArray = sellItemsArray;
            this.sellStartingTime = sellStartingTime;
            this.sellAuctionDuration = sellAuctionDuration;
            this.sellOptions = sellOptions;
            this.localIds = localIds;
            this.sellProlongOptions = sellProlongOptions;
        }
    }
}