﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PaymentItemsStruct
    {
        /// <remarks />
        public long payTransItId { get; set; }

        /// <remarks />
        public string payTransItName { get; set; }

        /// <remarks />
        public int payTransItCount { get; set; }

        /// <remarks />
        public float payTransItPrice { get; set; }
    }
}