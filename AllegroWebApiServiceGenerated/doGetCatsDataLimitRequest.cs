﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetCatsDataLimitRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetCatsDataLimitRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int countryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long localVersion;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int offset;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public bool onlyLeaf;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int packageElement;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string webapiKey;

        public doGetCatsDataLimitRequest()
        {
        }

        public doGetCatsDataLimitRequest(int countryId, long localVersion, string webapiKey, int offset,
            int packageElement, bool onlyLeaf)
        {
            this.countryId = countryId;
            this.localVersion = localVersion;
            this.webapiKey = webapiKey;
            this.offset = offset;
            this.packageElement = packageElement;
            this.onlyLeaf = onlyLeaf;
        }
    }
}