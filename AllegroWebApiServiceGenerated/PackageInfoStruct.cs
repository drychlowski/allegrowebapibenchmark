﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PackageInfoStruct
    {
        /// <remarks />
        public int operatorId { get; set; }

        /// <remarks />
        public string packageId { get; set; }

        /// <remarks />
        public string operatorName { get; set; }
    }
}