﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PharmacyRecipientDataStruct
    {
        /// <remarks />
        public string recipientFirstName { get; set; }

        /// <remarks />
        public string recipientLastName { get; set; }

        /// <remarks />
        public string recipientAddress { get; set; }

        /// <remarks />
        public string recipientPostcode { get; set; }

        /// <remarks />
        public string recipientCity { get; set; }
    }
}