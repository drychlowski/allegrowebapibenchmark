﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SellFormType
    {
        /// <remarks />
        public int sellFormId { get; set; }

        /// <remarks />
        public string sellFormTitle { get; set; }

        /// <remarks />
        public int sellFormCat { get; set; }

        /// <remarks />
        public int sellFormType { get; set; }

        /// <remarks />
        public int sellFormResType { get; set; }

        /// <remarks />
        public int sellFormDefValue { get; set; }

        /// <remarks />
        public int sellFormOpt { get; set; }

        /// <remarks />
        public int sellFormPos { get; set; }

        /// <remarks />
        public int sellFormLength { get; set; }

        /// <remarks />
        public string sellMinValue { get; set; }

        /// <remarks />
        public string sellMaxValue { get; set; }

        /// <remarks />
        public string sellFormDesc { get; set; }

        /// <remarks />
        public string sellFormOptsValues { get; set; }

        /// <remarks />
        public string sellFormFieldDesc { get; set; }

        /// <remarks />
        public int sellFormParamId { get; set; }

        /// <remarks />
        public string sellFormParamValues { get; set; }

        /// <remarks />
        public int sellFormParentId { get; set; }

        /// <remarks />
        public string sellFormParentValue { get; set; }

        /// <remarks />
        public string sellFormUnit { get; set; }

        /// <remarks />
        public int sellFormOptions { get; set; }
    }
}