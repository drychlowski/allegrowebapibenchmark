﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetMyPayoutsRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetMyPayoutsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long transCreateDateFrom;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long transCreateDateTo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int transOffset;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int transPageLimit;

        public doGetMyPayoutsRequest()
        {
        }

        public doGetMyPayoutsRequest(string sessionHandle, long transCreateDateFrom, long transCreateDateTo,
            int transPageLimit, int transOffset)
        {
            this.sessionHandle = sessionHandle;
            this.transCreateDateFrom = transCreateDateFrom;
            this.transCreateDateTo = transCreateDateTo;
            this.transPageLimit = transPageLimit;
            this.transOffset = transOffset;
        }
    }
}