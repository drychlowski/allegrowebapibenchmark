﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PhotoInfoType
    {
        /// <remarks />
        public string photoSize { get; set; }

        /// <remarks />
        public string photoUrl { get; set; }

        /// <remarks />
        public bool photoIsMain { get; set; }
    }
}