﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoMyBillingItemRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doMyBillingItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string option;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doMyBillingItemRequest()
        {
        }

        public doMyBillingItemRequest(string sessionHandle, long itemId, string option)
        {
            this.sessionHandle = sessionHandle;
            this.itemId = itemId;
            this.option = option;
        }
    }
}