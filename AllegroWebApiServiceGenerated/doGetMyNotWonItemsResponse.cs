﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyNotWonItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyNotWonItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int notWonItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public NotWonItemStruct[] notWonItemsList;

        public doGetMyNotWonItemsResponse()
        {
        }

        public doGetMyNotWonItemsResponse(int notWonItemsCounter, NotWonItemStruct[] notWonItemsList)
        {
            this.notWonItemsCounter = notWonItemsCounter;
            this.notWonItemsList = notWonItemsList;
        }
    }
}