﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserSentToDataStruct
    {
        /// <remarks />
        public long userId { get; set; }

        /// <remarks />
        public string userFirstName { get; set; }

        /// <remarks />
        public string userLastName { get; set; }

        /// <remarks />
        public string userCompany { get; set; }

        /// <remarks />
        public int userCountryId { get; set; }

        /// <remarks />
        public string userPostcode { get; set; }

        /// <remarks />
        public string userCity { get; set; }

        /// <remarks />
        public string userAddress { get; set; }
    }
}