﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ItemPostBuyDataStruct
    {
        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public UserPostBuyDataStruct[] usersPostBuyData { get; set; }
    }
}