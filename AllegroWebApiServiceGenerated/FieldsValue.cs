﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FieldsValue
    {
        /// <remarks />
        public int fid { get; set; }

        /// <remarks />
        public string fvalueString { get; set; }

        /// <remarks />
        public int fvalueInt { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool fvalueIntSpecified { get; set; }

        /// <remarks />
        public float fvalueFloat { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool fvalueFloatSpecified { get; set; }

        /// <remarks />
        [XmlElement(DataType = "base64Binary")]
        public byte[] fvalueImage { get; set; }

        /// <remarks />
        public long fvalueDatetime { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool fvalueDatetimeSpecified { get; set; }

        /// <remarks />
        public string fvalueDate { get; set; }

        /// <remarks />
        public RangeIntValueStruct fvalueRangeInt { get; set; }

        /// <remarks />
        public RangeFloatValueStruct fvalueRangeFloat { get; set; }

        /// <remarks />
        public RangeDateValueStruct fvalueRangeDate { get; set; }
    }
}