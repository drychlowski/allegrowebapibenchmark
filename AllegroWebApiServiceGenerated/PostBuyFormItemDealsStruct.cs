﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormItemDealsStruct
    {
        /// <remarks />
        public long dealId { get; set; }

        /// <remarks />
        public float dealFinalPrice { get; set; }

        /// <remarks />
        public int dealQuantity { get; set; }

        /// <remarks />
        public DateTime dealDate { get; set; }

        /// <remarks />
        public bool dealWasDiscounted { get; set; }

        /// <remarks />
        public PostBuyFormItemDealsVariantStruct dealVariant { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormItemDealsAdditionalServiceStruct[] dealAdditionalServices { get; set; }
    }
}