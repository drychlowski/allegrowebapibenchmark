﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetMyWonItemsRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetMyWonItemsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int categoryId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] itemIds;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int pageNumber;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int pageSize;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public string searchValue;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public SortOptionsStruct sortOptions;

        public doGetMyWonItemsRequest()
        {
        }

        public doGetMyWonItemsRequest(string sessionId, SortOptionsStruct sortOptions, string searchValue,
            int categoryId, long[] itemIds, int pageSize, int pageNumber)
        {
            this.sessionId = sessionId;
            this.sortOptions = sortOptions;
            this.searchValue = searchValue;
            this.categoryId = categoryId;
            this.itemIds = itemIds;
            this.pageSize = pageSize;
            this.pageNumber = pageNumber;
        }
    }
}