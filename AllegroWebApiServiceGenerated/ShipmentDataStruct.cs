﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ShipmentDataStruct
    {
        /// <remarks />
        public int shipmentId { get; set; }

        /// <remarks />
        public string shipmentName { get; set; }

        /// <remarks />
        public int shipmentType { get; set; }

        /// <remarks />
        public ShipmentTimeStruct shipmentTime { get; set; }
    }
}