﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetFilledPostBuyFormsRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetFilledPostBuyFormsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public long fillingTimeFrom;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long fillingTimeTo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int paymentType;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int userRole;

        public doGetFilledPostBuyFormsRequest()
        {
        }

        public doGetFilledPostBuyFormsRequest(string sessionId, int paymentType, int userRole, long fillingTimeFrom,
            long fillingTimeTo)
        {
            this.sessionId = sessionId;
            this.paymentType = paymentType;
            this.userRole = userRole;
            this.fillingTimeFrom = fillingTimeFrom;
            this.fillingTimeTo = fillingTimeTo;
        }
    }
}