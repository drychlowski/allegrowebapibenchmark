﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doCancelBidItemResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doCancelBidItemResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int cancelBidValue;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] cancelledBids;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public int[] notCancelledBids;

        public doCancelBidItemResponse()
        {
        }

        public doCancelBidItemResponse(int cancelBidValue, int[] cancelledBids, int[] notCancelledBids)
        {
            this.cancelBidValue = cancelBidValue;
            this.cancelledBids = cancelledBids;
            this.notCancelledBids = notCancelledBids;
        }
    }
}