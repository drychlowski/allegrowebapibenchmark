﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doAddPackageInfoToPostBuyFormResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doAddPackageInfoToPostBuyFormResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public PostBuyFormPackageInfoStruct postBuyFormPackageInfo;

        public doAddPackageInfoToPostBuyFormResponse()
        {
        }

        public doAddPackageInfoToPostBuyFormResponse(PostBuyFormPackageInfoStruct postBuyFormPackageInfo)
        {
            this.postBuyFormPackageInfo = postBuyFormPackageInfo;
        }
    }
}