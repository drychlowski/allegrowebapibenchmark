﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ProductParametersGroupStruct
    {
        /// <remarks />
        public string productParametersGroupName { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public ProductParametersStruct[] productParametersList { get; set; }
    }
}