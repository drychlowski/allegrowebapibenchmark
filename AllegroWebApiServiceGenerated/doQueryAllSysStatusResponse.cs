﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doQueryAllSysStatusResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    [XmlRoot("doQueryAllSysStatusResponse", Namespace = "https://webapi.allegro.pl/service.php")]
    public class doQueryAllSysStatusResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public SysStatusType[] sysCountryStatus;

        public doQueryAllSysStatusResponse()
        {
        }

        public doQueryAllSysStatusResponse(SysStatusType[] sysCountryStatus)
        {
            this.sysCountryStatus = sysCountryStatus;
        }
    }
}