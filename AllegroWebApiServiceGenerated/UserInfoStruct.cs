﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserInfoStruct
    {
        /// <remarks />
        public int userId { get; set; }

        /// <remarks />
        public string userLogin { get; set; }

        /// <remarks />
        public int userRating { get; set; }

        /// <remarks />
        public int userIcons { get; set; }

        /// <remarks />
        public int userCountry { get; set; }
    }
}