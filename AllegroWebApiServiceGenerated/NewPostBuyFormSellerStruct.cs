﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class NewPostBuyFormSellerStruct
    {
        /// <remarks />
        public int sellerId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public long[] sellerItemIds { get; set; }

        /// <remarks />
        public int sellerShipmentId { get; set; }

        /// <remarks />
        public int sellerGdId { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool sellerGdIdSpecified { get; set; }

        /// <remarks />
        public float sellerShipmentAmount { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool sellerShipmentAmountSpecified { get; set; }

        /// <remarks />
        public string sellerMessageTo { get; set; }
    }
}