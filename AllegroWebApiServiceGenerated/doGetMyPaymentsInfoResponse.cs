﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetMyPaymentsInfoResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyPaymentsInfoResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public PaymentsInfoStruct paymentsInfo;

        public doGetMyPaymentsInfoResponse()
        {
        }

        public doGetMyPaymentsInfoResponse(PaymentsInfoStruct paymentsInfo)
        {
            this.paymentsInfo = paymentsInfo;
        }
    }
}