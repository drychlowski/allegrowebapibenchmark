﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class RefundDetailsType
    {
        /// <remarks />
        public int refundId { get; set; }

        /// <remarks />
        public string refundStatus { get; set; }

        /// <remarks />
        public int refundQuantity { get; set; }

        /// <remarks />
        public DateTime createdDate { get; set; }

        /// <remarks />
        public DateTime considerDate { get; set; }
    }
}