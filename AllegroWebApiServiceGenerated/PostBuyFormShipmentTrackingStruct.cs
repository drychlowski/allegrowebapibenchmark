﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormShipmentTrackingStruct
    {
        /// <remarks />
        public int postBuyFormOperatorId { get; set; }

        /// <remarks />
        public string postBuyFormOperatorName { get; set; }

        /// <remarks />
        public string postBuyFormPackageId { get; set; }

        /// <remarks />
        public string postBuyFormPackageStatus { get; set; }
    }
}