﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SiteJournal
    {
        /// <remarks />
        public long rowId { get; set; }

        /// <remarks />
        public long itemId { get; set; }

        /// <remarks />
        public string changeType { get; set; }

        /// <remarks />
        public long changeDate { get; set; }

        /// <remarks />
        public float currentPrice { get; set; }

        /// <remarks />
        public long itemSellerId { get; set; }
    }
}