﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyNotSoldItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyNotSoldItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int notSoldItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public NotSoldItemStruct[] notSoldItemsList;

        public doGetMyNotSoldItemsResponse()
        {
        }

        public doGetMyNotSoldItemsResponse(int notSoldItemsCounter, NotSoldItemStruct[] notSoldItemsList)
        {
            this.notSoldItemsCounter = notSoldItemsCounter;
            this.notSoldItemsList = notSoldItemsList;
        }
    }
}