﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class AfterSalesServiceConditionsStruct
    {
        /// <remarks />
        public string impliedWarranty { get; set; }

        /// <remarks />
        public string returnPolicy { get; set; }

        /// <remarks />
        public string warranty { get; set; }
    }
}