﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SellFilterOptionsStruct
    {
        /// <remarks />
        public int filterFormat { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterFormatSpecified { get; set; }

        /// <remarks />
        public int filterBids { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterBidsSpecified { get; set; }

        /// <remarks />
        public int filterToEnd { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterToEndSpecified { get; set; }

        /// <remarks />
        public int filterFromStart { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterFromStartSpecified { get; set; }

        /// <remarks />
        public int filterAutoListing { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterAutoListingSpecified { get; set; }

        /// <remarks />
        public FilterPriceStruct filterPrice { get; set; }

        /// <remarks />
        public int filterDurationType { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool filterDurationTypeSpecified { get; set; }
    }
}