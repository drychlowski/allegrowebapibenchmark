﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormPackageInfoStruct
    {
        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] packageIdsAdded { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] packageIdsNotAddedIncorrectOperatorId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] packageIdsNotAddedIncorrectPackageId { get; set; }
    }
}