﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CategoriesListType
    {
        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public CategoryTreeType[] categoriesTree { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public CategoryPathType[] categoriesPath { get; set; }
    }
}