﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PaymentsUserDataStruct
    {
        /// <remarks />
        public string userFullName { get; set; }

        /// <remarks />
        public string userAddress { get; set; }

        /// <remarks />
        public string userPostcode { get; set; }

        /// <remarks />
        public string userCity { get; set; }

        /// <remarks />
        public int userCountry { get; set; }

        /// <remarks />
        public string userPhone { get; set; }
    }
}