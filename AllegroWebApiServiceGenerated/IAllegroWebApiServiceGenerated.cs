﻿using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [ServiceContract(Namespace = "https://webapi.allegro.pl/service.php", ConfigurationName = "AllegroWebApiService")]
    public interface IAllegroWebApiServiceGenerated : IAllegroWebApiService
    {
        [OperationContract(Action = "#doAddPackageInfoToPostBuyForm", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doAddPackageInfoToPostBuyFormResponse> doAddPackageInfoToPostBuyFormAsync(
            doAddPackageInfoToPostBuyFormRequest request);

        [OperationContract(Action = "#doAddToBlackList", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doAddToBlackListResponse> doAddToBlackListAsync(doAddToBlackListRequest request);

        [OperationContract(Action = "#doBidItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doBidItemResponse> doBidItemAsync(doBidItemRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doCancelBidItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCancelBidItemResponse> doCancelBidItemAsync(doCancelBidItemRequest request);

        [OperationContract(Action = "#doCancelRefundForm", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCancelRefundFormResponse> doCancelRefundFormAsync(doCancelRefundFormRequest request);

        [OperationContract(Action = "#doCancelRefundWarning", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCancelRefundWarningResponse> doCancelRefundWarningAsync(doCancelRefundWarningRequest request);

        [OperationContract(Action = "#doCancelTransaction", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCancelTransactionResponse> doCancelTransactionAsync(doCancelTransactionRequest request);

        [OperationContract(Action = "#doChangeItemFields", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doChangeItemFieldsResponse> doChangeItemFieldsAsync(doChangeItemFieldsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doChangePriceItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doChangePriceItemResponse> doChangePriceItemAsync(doChangePriceItemRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doChangeQuantityItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doChangeQuantityItemResponse> doChangeQuantityItemAsync(doChangeQuantityItemRequest request);

        [OperationContract(Action = "#doCheckItemDescription", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCheckItemDescriptionResponse> doCheckItemDescriptionAsync(doCheckItemDescriptionRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doCheckNewAuctionExt", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doCheckNewAuctionExtResponse> doCheckNewAuctionExtAsync(doCheckNewAuctionExtRequest request);

        [OperationContract(Action = "#doFinishItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doFinishItemResponse> doFinishItemAsync(doFinishItemRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doFinishItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doFinishItemsResponse> doFinishItemsAsync(doFinishItemsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetArchiveRefundsList", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetArchiveRefundsListResponse> doGetArchiveRefundsListAsync(doGetArchiveRefundsListRequest request);

        [OperationContract(Action = "#doGetBidItem2", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        new Task<doGetBidItem2Response> doGetBidItem2Async(doGetBidItem2Request request);

        [OperationContract(Action = "#doGetBlackListUsers", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetBlackListUsersResponse> doGetBlackListUsersAsync(doGetBlackListUsersRequest request);

        [OperationContract(Action = "#doGetCategoryPath", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetCategoryPathResponse> doGetCategoryPathAsync(doGetCategoryPathRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetCatsData", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetCatsDataResponse> doGetCatsDataAsync(doGetCatsDataRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetCatsDataCount", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetCatsDataCountResponse> doGetCatsDataCountAsync(doGetCatsDataCountRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetCatsDataLimit", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetCatsDataLimitResponse> doGetCatsDataLimitAsync(doGetCatsDataLimitRequest request);

        [OperationContract(Action = "#doGetCountries", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetCountriesResponse> doGetCountriesAsync(doGetCountriesRequest request);

        [OperationContract(Action = "#doGetDeals", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetDealsResponse> doGetDealsAsync(doGetDealsRequest request);

        [OperationContract(Action = "#doGetFilledPostBuyForms", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetFilledPostBuyFormsResponse> doGetFilledPostBuyFormsAsync(doGetFilledPostBuyFormsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetFreeDeliveryAmount", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetFreeDeliveryAmountResponse> doGetFreeDeliveryAmountAsync(doGetFreeDeliveryAmountRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetItemFields", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetItemFieldsResponse> doGetItemFieldsAsync(doGetItemFieldsRequest request);

        [OperationContract(Action = "#doGetItemsImages", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetItemsImagesResponse> doGetItemsImagesAsync(doGetItemsImagesRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetItemsInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        new Task<doGetItemsInfoResponse> doGetItemsInfoAsync(doGetItemsInfoRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetItemsList", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetItemsListResponse> doGetItemsListAsync(doGetItemsListRequest request);

        [OperationContract(Action = "#doGetMyAddresses", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyAddressesResponse> doGetMyAddressesAsync(doGetMyAddressesRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyBidItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyBidItemsResponse> doGetMyBidItemsAsync(doGetMyBidItemsRequest request);

        [OperationContract(Action = "#doGetMyCurrentShipmentPriceType", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyCurrentShipmentPriceTypeResponse> doGetMyCurrentShipmentPriceTypeAsync(
            doGetMyCurrentShipmentPriceTypeRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyData", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyDataResponse> doGetMyDataAsync(doGetMyDataRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyFutureItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyFutureItemsResponse> doGetMyFutureItemsAsync(doGetMyFutureItemsRequest request);

        [OperationContract(Action = "#doGetMyIncomingPayments", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyIncomingPaymentsResponse> doGetMyIncomingPaymentsAsync(doGetMyIncomingPaymentsRequest request);

        [OperationContract(Action = "#doGetMyIncomingPaymentsRefunds", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyIncomingPaymentsRefundsResponse> doGetMyIncomingPaymentsRefundsAsync(
            doGetMyIncomingPaymentsRefundsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyNotSoldItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyNotSoldItemsResponse> doGetMyNotSoldItemsAsync(doGetMyNotSoldItemsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyNotWonItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyNotWonItemsResponse> doGetMyNotWonItemsAsync(doGetMyNotWonItemsRequest request);

        [OperationContract(Action = "#doGetMyPayments", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyPaymentsResponse> doGetMyPaymentsAsync(doGetMyPaymentsRequest request);

        [OperationContract(Action = "#doGetMyPaymentsInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyPaymentsInfoResponse> doGetMyPaymentsInfoAsync(doGetMyPaymentsInfoRequest request);

        [OperationContract(Action = "#doGetMyPaymentsRefunds", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyPaymentsRefundsResponse> doGetMyPaymentsRefundsAsync(doGetMyPaymentsRefundsRequest request);

        [OperationContract(Action = "#doGetMyPayouts", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyPayoutsResponse> doGetMyPayoutsAsync(doGetMyPayoutsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyPayoutsDetails", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyPayoutsDetailsResponse> doGetMyPayoutsDetailsAsync(doGetMyPayoutsDetailsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMySellItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMySellItemsResponse> doGetMySellItemsAsync(doGetMySellItemsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMySoldItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMySoldItemsResponse> doGetMySoldItemsAsync(doGetMySoldItemsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetMyWonItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetMyWonItemsResponse> doGetMyWonItemsAsync(doGetMyWonItemsRequest request);

        [OperationContract(Action = "#doGetPaymentMethods", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPaymentMethodsResponse> doGetPaymentMethodsAsync(doGetPaymentMethodsRequest request);

        [OperationContract(Action = "#doGetPostBuyData", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPostBuyDataResponse> doGetPostBuyDataAsync(doGetPostBuyDataRequest request);

        [OperationContract(Action = "#doGetPostBuyFormsDataForBuyers", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPostBuyFormsDataForBuyersResponse> doGetPostBuyFormsDataForBuyersAsync(
            doGetPostBuyFormsDataForBuyersRequest request);

        [OperationContract(Action = "#doGetPostBuyFormsDataForSellers", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPostBuyFormsDataForSellersResponse> doGetPostBuyFormsDataForSellersAsync(
            doGetPostBuyFormsDataForSellersRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetPostBuyFormsIds", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPostBuyFormsIdsResponse> doGetPostBuyFormsIdsAsync(doGetPostBuyFormsIdsRequest request);

        [OperationContract(Action = "#doGetPostBuyItemInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetPostBuyItemInfoResponse> doGetPostBuyItemInfoAsync(doGetPostBuyItemInfoRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetRefundsDeals", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetRefundsDealsResponse> doGetRefundsDealsAsync(doGetRefundsDealsRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetRefundsList", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetRefundsListResponse> doGetRefundsListAsync(doGetRefundsListRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetRefundsReasons", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetRefundsReasonsResponse> doGetRefundsReasonsAsync(doGetRefundsReasonsRequest request);

        [OperationContract(Action = "#doGetRelatedItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetRelatedItemsResponse> doGetRelatedItemsAsync(doGetRelatedItemsRequest request);

        [OperationContract(Action = "#doGetSellFormFieldsForCategory", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSellFormFieldsForCategoryResponse> doGetSellFormFieldsForCategoryAsync(
            doGetSellFormFieldsForCategoryRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetSessionHandleForWidget", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSessionHandleForWidgetResponse> doGetSessionHandleForWidgetAsync(
            doGetSessionHandleForWidgetRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doGetShipmentData", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetShipmentDataResponse> doGetShipmentDataAsync(doGetShipmentDataRequest request);

        [OperationContract(Action = "#doGetShipmentDataForRelatedItems", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetShipmentDataForRelatedItemsResponse> doGetShipmentDataForRelatedItemsAsync(
            doGetShipmentDataForRelatedItemsRequest request);

        [OperationContract(Action = "#doGetShipmentPriceTypes", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetShipmentPriceTypesResponse> doGetShipmentPriceTypesAsync(doGetShipmentPriceTypesRequest request);

        [OperationContract(Action = "#doGetSiteJournal", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        new Task<doGetSiteJournalResponse> doGetSiteJournalAsync(doGetSiteJournalRequest request);

        [OperationContract(Action = "#doGetSiteJournalDeals", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSiteJournalDealsResponse> doGetSiteJournalDealsAsync(doGetSiteJournalDealsRequest request);

        [OperationContract(Action = "#doGetSiteJournalDealsInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSiteJournalDealsInfoResponse>
            doGetSiteJournalDealsInfoAsync(doGetSiteJournalDealsInfoRequest request);

        [OperationContract(Action = "#doGetSiteJournalInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSiteJournalInfoResponse> doGetSiteJournalInfoAsync(doGetSiteJournalInfoRequest request);

        [OperationContract(Action = "#doGetStatesInfo", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetStatesInfoResponse> doGetStatesInfoAsync(doGetStatesInfoRequest request);

        [OperationContract(Action = "#doGetSystemTime", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetSystemTimeResponse> doGetSystemTimeAsync(doGetSystemTimeRequest request);

        [OperationContract(Action = "#doGetTransactionsIDs", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetTransactionsIDsResponse> doGetTransactionsIDsAsync(doGetTransactionsIDsRequest request);

        [OperationContract(Action = "#doGetUserID", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetUserIDResponse> doGetUserIDAsync(doGetUserIDRequest request);

        [OperationContract(Action = "#doGetUserLogin", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doGetUserLoginResponse> doGetUserLoginAsync(doGetUserLoginRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doLogin", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doLoginResponse> doLoginAsync(doLoginRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doLoginEnc", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        new Task<doLoginEncResponse> doLoginEncAsync(doLoginEncRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doLoginWithAccessToken", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doLoginWithAccessTokenResponse> doLoginWithAccessTokenAsync(doLoginWithAccessTokenRequest request);

        [OperationContract(Action = "#doMyAccount2", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doMyAccount2Response> doMyAccount2Async(doMyAccount2Request request);

        [OperationContract(Action = "#doMyAccountItemsCount", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doMyAccountItemsCountResponse> doMyAccountItemsCountAsync(doMyAccountItemsCountRequest request);

        [OperationContract(Action = "#doMyBilling", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doMyBillingResponse> doMyBillingAsync(doMyBillingRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doMyBillingItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doMyBillingItemResponse> doMyBillingItemAsync(doMyBillingItemRequest request);

        [OperationContract(Action = "#doMyContact", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doMyContactResponse> doMyContactAsync(doMyContactRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doNewAuctionExt", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doNewAuctionExtResponse> doNewAuctionExtAsync(doNewAuctionExtRequest request);

        [OperationContract(Action = "#doQueryAllSysStatus", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        new Task<doQueryAllSysStatusResponse> doQueryAllSysStatusAsync(doQueryAllSysStatusRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doQuerySysStatus", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doQuerySysStatusResponse> doQuerySysStatusAsync(doQuerySysStatusRequest request);

        [OperationContract(Action = "#doRemoveFromBlackList", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doRemoveFromBlackListResponse> doRemoveFromBlackListAsync(doRemoveFromBlackListRequest request);

        [OperationContract(Action = "#doRequestCancelBid", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doRequestCancelBidResponse> doRequestCancelBidAsync(doRequestCancelBidRequest request);

        [OperationContract(Action = "#doRequestPayout", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doRequestPayoutResponse> doRequestPayoutAsync(doRequestPayoutRequest request);

        [OperationContract(Action = "#doRequestSurcharge", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doRequestSurchargeResponse> doRequestSurchargeAsync(doRequestSurchargeRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doSellSomeAgain", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSellSomeAgainResponse> doSellSomeAgainAsync(doSellSomeAgainRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doSellSomeAgainInShop", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSellSomeAgainInShopResponse> doSellSomeAgainInShopAsync(doSellSomeAgainInShopRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doSendEmailToUser", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSendEmailToUserResponse> doSendEmailToUserAsync(doSendEmailToUserRequest request);

        [OperationContract(Action = "#doSendPostBuyForm", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSendPostBuyFormResponse> doSendPostBuyFormAsync(doSendPostBuyFormRequest request);

        [OperationContract(Action = "#doSendRefundForm", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSendRefundFormResponse> doSendRefundFormAsync(doSendRefundFormRequest request);

        [OperationContract(Action = "#doSetFreeDeliveryAmount", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSetFreeDeliveryAmountResponse> doSetFreeDeliveryAmountAsync(doSetFreeDeliveryAmountRequest request);

        [OperationContract(Action = "#doSetShipmentPriceType", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doSetShipmentPriceTypeResponse> doSetShipmentPriceTypeAsync(doSetShipmentPriceTypeRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doShowItemInfoExt", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doShowItemInfoExtResponse> doShowItemInfoExtAsync(doShowItemInfoExtRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doShowUser", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doShowUserResponse> doShowUserAsync(doShowUserRequest request);

        // CODEGEN: Generating message contract since the operation has multiple return values.
        [OperationContract(Action = "#doVerifyItem", ReplyAction = "*")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<doVerifyItemResponse> doVerifyItemAsync(doVerifyItemRequest request);
    }
}