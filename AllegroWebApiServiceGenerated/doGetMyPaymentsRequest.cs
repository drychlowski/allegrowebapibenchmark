﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetMyPaymentsRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doGetMyPaymentsRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long itemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public int pageNumber;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public int pageSize;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public long paymentTimeFrom;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long paymentTimeTo;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int sellerId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 7)]
        public int strictedSearch;

        public doGetMyPaymentsRequest()
        {
        }

        public doGetMyPaymentsRequest(string sessionId, int sellerId, long itemId, long paymentTimeFrom,
            long paymentTimeTo, int pageSize, int pageNumber, int strictedSearch)
        {
            this.sessionId = sessionId;
            this.sellerId = sellerId;
            this.itemId = itemId;
            this.paymentTimeFrom = paymentTimeFrom;
            this.paymentTimeTo = paymentTimeTo;
            this.pageSize = pageSize;
            this.pageNumber = pageNumber;
            this.strictedSearch = strictedSearch;
        }
    }
}