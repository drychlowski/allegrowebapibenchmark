﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "doGetSiteJournalResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    [XmlRoot("doGetSiteJournalResponse", Namespace = "https://webapi.allegro.pl/service.php")]
    public class doGetSiteJournalResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public SiteJournal[] siteJournalArray;

        public doGetSiteJournalResponse()
        {
        }

        public doGetSiteJournalResponse(SiteJournal[] siteJournalArray)
        {
            this.siteJournalArray = siteJournalArray;
        }
    }
}