﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FilterRelationType
    {
        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] relationAnd { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] relationOr { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] relationExclude { get; set; }
    }
}