﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doCheckNewAuctionExtResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doCheckNewAuctionExtResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int itemIsAllegroStandard;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string itemPrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public string itemPriceDesc;

        public doCheckNewAuctionExtResponse()
        {
        }

        public doCheckNewAuctionExtResponse(string itemPrice, string itemPriceDesc, int itemIsAllegroStandard)
        {
            this.itemPrice = itemPrice;
            this.itemPriceDesc = itemPriceDesc;
            this.itemIsAllegroStandard = itemIsAllegroStandard;
        }
    }
}