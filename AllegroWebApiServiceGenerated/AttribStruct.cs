﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class AttribStruct
    {
        /// <remarks />
        public string attribName { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public string[] attribValues { get; set; }
    }
}