﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class CategoryData
    {
        /// <remarks />
        public int catId { get; set; }

        /// <remarks />
        public int catParent { get; set; }

        /// <remarks />
        public int catCountry { get; set; }

        /// <remarks />
        public int catLevel { get; set; }

        /// <remarks />
        public int catIsLeaf { get; set; }

        /// <remarks />
        public string catName { get; set; }

        /// <remarks />
        public int catOptions { get; set; }
    }
}