﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMyBidItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMyBidItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int bidItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public BidItemStruct[] bidItemsList;

        public doGetMyBidItemsResponse()
        {
        }

        public doGetMyBidItemsResponse(int bidItemsCounter, BidItemStruct[] bidItemsList)
        {
            this.bidItemsCounter = bidItemsCounter;
            this.bidItemsList = bidItemsList;
        }
    }
}