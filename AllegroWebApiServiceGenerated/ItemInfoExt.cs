﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class ItemInfoExt
    {
        /// <remarks />
        public long itId { get; set; }

        /// <remarks />
        public int itCountry { get; set; }

        /// <remarks />
        public string itName { get; set; }

        /// <remarks />
        public float itPrice { get; set; }

        /// <remarks />
        public int itBidCount { get; set; }

        /// <remarks />
        public long itEndingTime { get; set; }

        /// <remarks />
        public long itSellerId { get; set; }

        /// <remarks />
        public string itSellerLogin { get; set; }

        /// <remarks />
        public int itSellerRating { get; set; }

        /// <remarks />
        public long itStartingTime { get; set; }

        /// <remarks />
        public float itStartingPrice { get; set; }

        /// <remarks />
        public int itQuantity { get; set; }

        /// <remarks />
        public int itFotoCount { get; set; }

        /// <remarks />
        public float itReservePrice { get; set; }

        /// <remarks />
        public string itLocation { get; set; }

        /// <remarks />
        public float itBuyNowPrice { get; set; }

        /// <remarks />
        public int itBuyNowActive { get; set; }

        /// <remarks />
        public float itAdvertisementPrice { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool itAdvertisementPriceSpecified { get; set; }

        /// <remarks />
        public int itAdvertisementActive { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool itAdvertisementActiveSpecified { get; set; }

        /// <remarks />
        public int itHighBidder { get; set; }

        /// <remarks />
        public string itHighBidderLogin { get; set; }

        /// <remarks />
        public string itDescription { get; set; }

        /// <remarks />
        public string itStandardizedDescription { get; set; }

        /// <remarks />
        public int itOptions { get; set; }

        /// <remarks />
        public int itState { get; set; }

        /// <remarks />
        public float itWireTransfer { get; set; }

        /// <remarks />
        public float itPostDelivery { get; set; }

        /// <remarks />
        public string itPostInfo { get; set; }

        /// <remarks />
        public int itQuantityType { get; set; }

        /// <remarks />
        public int itIsEco { get; set; }

        /// <remarks />
        public long itHitCount { get; set; }

        /// <remarks />
        public string itPostcode { get; set; }

        /// <remarks />
        public int itVatInvoice { get; set; }

        /// <remarks />
        public int itVatMarginInvoice { get; set; }

        /// <remarks />
        public int itWithoutVatInvoice { get; set; }

        /// <remarks />
        public string itBankAccount1 { get; set; }

        /// <remarks />
        public string itBankAccount2 { get; set; }

        /// <remarks />
        public int itStartingQuantity { get; set; }

        /// <remarks />
        public int itIsForGuests { get; set; }

        /// <remarks />
        public int itHasProduct { get; set; }

        /// <remarks />
        public int itOrderFulfillmentTime { get; set; }

        /// <remarks />
        public int itEndingInfo { get; set; }

        /// <remarks />
        public int itIsAllegroStandard { get; set; }

        /// <remarks />
        public int itIsNewUsed { get; set; }

        /// <remarks />
        public int itIsBrandZone { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool itIsBrandZoneSpecified { get; set; }

        /// <remarks />
        public int itIsFulfillmentTimeActive { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool itIsFulfillmentTimeActiveSpecified { get; set; }

        /// <remarks />
        public string itEan { get; set; }

        /// <remarks />
        public string itContact { get; set; }
    }
}