﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserDataStruct
    {
        /// <remarks />
        public long userId { get; set; }

        /// <remarks />
        public string userLogin { get; set; }

        /// <remarks />
        public int userRating { get; set; }

        /// <remarks />
        public string userFirstName { get; set; }

        /// <remarks />
        public string userLastName { get; set; }

        /// <remarks />
        public string userMaidenName { get; set; }

        /// <remarks />
        public string userCompany { get; set; }

        /// <remarks />
        public int userCountryId { get; set; }

        /// <remarks />
        public int userStateId { get; set; }

        /// <remarks />
        public string userPostcode { get; set; }

        /// <remarks />
        public string userCity { get; set; }

        /// <remarks />
        public string userAddress { get; set; }

        /// <remarks />
        public string userEmail { get; set; }

        /// <remarks />
        public string userPhone { get; set; }

        /// <remarks />
        public string userPhone2 { get; set; }

        /// <remarks />
        public int userSsStatus { get; set; }

        /// <remarks />
        public int siteCountryId { get; set; }

        /// <remarks />
        public int userJuniorStatus { get; set; }

        /// <remarks />
        public long userBirthDate { get; set; }

        /// <remarks />
        public int userHasShop { get; set; }

        /// <remarks />
        public int userCompanyIcon { get; set; }

        /// <remarks />
        public int userIsAllegroStandard { get; set; }
    }
}