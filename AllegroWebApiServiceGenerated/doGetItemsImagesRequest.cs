﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoGetItemsImagesRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetItemsImagesRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public int imageType;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public ItemGetImage[] itemsArray;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doGetItemsImagesRequest()
        {
        }

        public doGetItemsImagesRequest(string sessionHandle, ItemGetImage[] itemsArray, int imageType)
        {
            this.sessionHandle = sessionHandle;
            this.itemsArray = itemsArray;
            this.imageType = imageType;
        }
    }
}