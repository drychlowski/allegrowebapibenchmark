﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doGetMySoldItemsResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetMySoldItemsResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public int soldItemsCounter;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public SoldItemStruct[] soldItemsList;

        public doGetMySoldItemsResponse()
        {
        }

        public doGetMySoldItemsResponse(int soldItemsCounter, SoldItemStruct[] soldItemsList)
        {
            this.soldItemsCounter = soldItemsCounter;
            this.soldItemsList = soldItemsList;
        }
    }
}