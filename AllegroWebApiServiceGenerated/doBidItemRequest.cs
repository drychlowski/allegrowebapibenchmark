﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [MessageContract(WrapperName = "DoBidItemRequest", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    public class doBidItemRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public long bidBuyNow;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long bidItId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public long bidQuantity;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public float bidUserPrice;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public PharmacyRecipientDataStruct pharmacyRecipientData;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 6)]
        public string variantId;

        public doBidItemRequest()
        {
        }

        public doBidItemRequest(string sessionHandle, long bidItId, float bidUserPrice, long bidQuantity,
            long bidBuyNow, PharmacyRecipientDataStruct pharmacyRecipientData, string variantId)
        {
            this.sessionHandle = sessionHandle;
            this.bidItId = bidItId;
            this.bidUserPrice = bidUserPrice;
            this.bidQuantity = bidQuantity;
            this.bidBuyNow = bidBuyNow;
            this.pharmacyRecipientData = pharmacyRecipientData;
            this.variantId = variantId;
        }
    }
}