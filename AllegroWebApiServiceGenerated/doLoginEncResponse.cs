﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doLoginEncResponse", WrapperNamespace = "https://webapi.allegro.pl/service.php",
        IsWrapped = true)]
    [XmlRoot("doLoginEncResponse", Namespace = "https://webapi.allegro.pl/service.php")]
    public class doLoginEncResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long serverTime;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandlePart;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long userId;

        public doLoginEncResponse()
        {
        }

        public doLoginEncResponse(string sessionHandlePart, long userId, long serverTime)
        {
            this.sessionHandlePart = sessionHandlePart;
            this.userId = userId;
            this.serverTime = serverTime;
        }
    }
}