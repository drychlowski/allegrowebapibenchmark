﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class UserBlackListAddResultStruct
    {
        /// <remarks />
        public int userId { get; set; }

        /// <remarks />
        public int addToBlackListResult { get; set; }

        /// <remarks />
        public string addToBlackListErrCode { get; set; }

        /// <remarks />
        public string addToBlackListErrMsg { get; set; }
    }
}