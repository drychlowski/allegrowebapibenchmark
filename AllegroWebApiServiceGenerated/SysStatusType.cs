﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class SysStatusType
    {
        /// <remarks />
        public int countryId { get; set; }

        /// <remarks />
        public string programVersion { get; set; }

        /// <remarks />
        public string catsVersion { get; set; }

        /// <remarks />
        public string apiVersion { get; set; }

        /// <remarks />
        public string attribVersion { get; set; }

        /// <remarks />
        public string formSellVersion { get; set; }

        /// <remarks />
        public string siteVersion { get; set; }

        /// <remarks />
        public long verKey { get; set; }
    }
}