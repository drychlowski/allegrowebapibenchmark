﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoGetSessionHandleForWidgetRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doGetSessionHandleForWidgetRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public int countryCode;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string webapiKey;

        public doGetSessionHandleForWidgetRequest()
        {
        }

        public doGetSessionHandleForWidgetRequest(string webapiKey, int countryCode)
        {
            this.webapiKey = webapiKey;
            this.countryCode = countryCode;
        }
    }
}