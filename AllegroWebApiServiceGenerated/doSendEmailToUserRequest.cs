﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "DoSendEmailToUserRequest",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSendEmailToUserRequest
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        public long mailToUserItemId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 5)]
        public string mailToUserMessage;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 4)]
        public int mailToUserOption;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        public long mailToUserReceiverId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 3)]
        public int mailToUserSubjectId;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        public string sessionHandle;

        public doSendEmailToUserRequest()
        {
        }

        public doSendEmailToUserRequest(string sessionHandle, long mailToUserItemId, long mailToUserReceiverId,
            int mailToUserSubjectId, int mailToUserOption, string mailToUserMessage)
        {
            this.sessionHandle = sessionHandle;
            this.mailToUserItemId = mailToUserItemId;
            this.mailToUserReceiverId = mailToUserReceiverId;
            this.mailToUserSubjectId = mailToUserSubjectId;
            this.mailToUserOption = mailToUserOption;
            this.mailToUserMessage = mailToUserMessage;
        }
    }
}