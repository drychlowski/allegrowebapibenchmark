﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    [DebuggerStepThrough]
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [MessageContract(WrapperName = "doSellSomeAgainInShopResponse",
        WrapperNamespace = "https://webapi.allegro.pl/service.php", IsWrapped = true)]
    public class doSellSomeAgainInShopResponse
    {
        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 0)]
        [XmlArrayItem("item", IsNullable = false)]
        public StructSellAgain[] itemsSellAgain;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 1)]
        [XmlArrayItem("item", IsNullable = false)]
        public StructSellFailed[] itemsSellFailed;

        [MessageBodyMember(Namespace = "https://webapi.allegro.pl/service.php", Order = 2)]
        [XmlArrayItem("item", IsNullable = false)]
        public long[] itemsSellNotFound;

        public doSellSomeAgainInShopResponse()
        {
        }

        public doSellSomeAgainInShopResponse(StructSellAgain[] itemsSellAgain, StructSellFailed[] itemsSellFailed,
            long[] itemsSellNotFound)
        {
            this.itemsSellAgain = itemsSellAgain;
            this.itemsSellFailed = itemsSellFailed;
            this.itemsSellNotFound = itemsSellNotFound;
        }
    }
}