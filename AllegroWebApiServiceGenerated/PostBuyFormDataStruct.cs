﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class PostBuyFormDataStruct
    {
        /// <remarks />
        public long postBuyFormId { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormItemStruct[] postBuyFormItems { get; set; }

        /// <remarks />
        public long postBuyFormBuyerId { get; set; }

        /// <remarks />
        public float postBuyFormAmount { get; set; }

        /// <remarks />
        public float postBuyFormPostageAmount { get; set; }

        /// <remarks />
        public int postBuyFormInvoiceOption { get; set; }

        /// <remarks />
        public string postBuyFormMsgToSeller { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormInvoiceData { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormShipmentAddress { get; set; }

        /// <remarks />
        public string postBuyFormPayType { get; set; }

        /// <remarks />
        public long postBuyFormPayId { get; set; }

        /// <remarks />
        public string postBuyFormPayStatus { get; set; }

        /// <remarks />
        public string postBuyFormDateInit { get; set; }

        /// <remarks />
        public string postBuyFormDateRecv { get; set; }

        /// <remarks />
        public string postBuyFormDateCancel { get; set; }

        /// <remarks />
        public int postBuyFormShipmentId { get; set; }

        /// <remarks />
        public PostBuyFormAddressStruct postBuyFormGdAddress { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public PostBuyFormShipmentTrackingStruct[] postBuyFormShipmentTracking { get; set; }

        /// <remarks />
        [XmlArrayItem("item", IsNullable = false)]
        public long[] postBuyFormSurchargesList { get; set; }

        /// <remarks />
        public string postBuyFormGdAdditionalInfo { get; set; }

        /// <remarks />
        public float postBuyFormPaymentAmount { get; set; }

        /// <remarks />
        public int postBuyFormSentBySeller { get; set; }

        /// <remarks />
        public string postBuyFormBuyerLogin { get; set; }

        /// <remarks />
        public string postBuyFormBuyerEmail { get; set; }

        /// <remarks />
        public float postBuyFormAdditionalServicesAmount { get; set; }
    }
}