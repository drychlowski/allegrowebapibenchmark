﻿using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AllegroWebApiBenchmark.AllegroWebApiServiceGenerated
{
    /// <remarks />
    [GeneratedCode("dotnet-svcutil", "1.0.0.1")]
    [DebuggerStepThrough]
    [XmlType(Namespace = "https://webapi.allegro.pl/service.php")]
    public class FinishItemsStruct
    {
        /// <remarks />
        public long finishItemId { get; set; }

        /// <remarks />
        public int finishCancelAllBids { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool finishCancelAllBidsSpecified { get; set; }

        /// <remarks />
        public string finishCancelReason { get; set; }
    }
}