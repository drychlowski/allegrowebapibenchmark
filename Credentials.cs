﻿using System;
using System.IO;

namespace AllegroWebApiBenchmark
{
    class Credentials : ICredentials
    {
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string WebApiKey { get; private set; }

        public Credentials(string username, string password, string webApiKey)
        {
            Username = username;
            Password = password;
            WebApiKey = webApiKey;
        }

        public Credentials(string fileName)
        {
            if (LoadFromFile(fileName)) return;

            Username = string.Empty;
            Password = string.Empty;
            WebApiKey = string.Empty;
        }

        private bool LoadFromFile(string fileName)
        {
            string[] credentials;
            try
            {
                credentials = File.ReadAllLines(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                return false;
            }

            if (credentials.Length != 3)
            {
                Console.WriteLine("Error: wrong number of items in file. Credentials not loaded.");
                return false;
            }

            Username = credentials[0];
            Password = credentials[1];
            WebApiKey = credentials[2];
            return true;
        }
    }
}
